﻿using UnityEngine;
using System.Collections;

public class PlayersStats : MonoBehaviour
{
	//For the Globito stuff...
	private bool shooting;

	public bool isEnemy = false;

	private bool onGround;

	//Var PlayerMove Scrypt--->
	public float accelerationZ;
	public float accelerationX;
	public float maxSpeed;

	//Var PositionMovement Scrypt--->
	public float speedIA;
	public float maxSpeedIA;

	private float distToGround;


	void Start()
	{
		shooting = true;
		distToGround = GetComponent<Collider>().bounds.extents.y;
		onGround = true;
		accelerationZ = 1f;
		accelerationX = 1f;
        maxSpeed = 15f;
        speedIA = 0.5f;
		maxSpeedIA = 10f;
	}

	void OnTriggerStay(Collider col)
	{
		if(col.gameObject.tag == "plane")
		{
			onGround = true;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag == "plane")
		{
			onGround = false;
		}
	}

	public bool isOnGround()
	{
		return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
	}

	public bool getOnGround()
	{
		return this.onGround;
	}

	public float getAccelerationZ()
	{
		return accelerationZ;
	}

	public float getAccelerationX()
	{
		return accelerationX;
	}

	public float getMaxSpeed()
	{
		return maxSpeed;
	}

	public void accelerateZ(float newAcceleration)
	{
		accelerationZ += newAcceleration;
	}

	public void accelerateX(float newAcceleration)
	{
		accelerationX += newAcceleration;
	}

	public void setAccelerationZ(float newAcceleration)
	{
		accelerationZ = newAcceleration;
	}

	public void setAccelerationX(float newAcceleration)
	{
		accelerationX = newAcceleration;
	}

	public void setMaxSpeed(float newMaxSpeed)
	{
		maxSpeed = newMaxSpeed;
	}

	public float getSpeedIA()
	{
		return speedIA;
	}

	public float getMaxSpeedIA()
	{
		return maxSpeedIA;
	}

	public void setSpeedIA(float newSpeedIA)
	{
		speedIA = newSpeedIA;
	}

	public void setMaxSpeedIA(float newMaxSpeedIA)
	{
		maxSpeedIA = newMaxSpeedIA;
	}

	public void accelerateIA(float newAcceleration)
	{
		speedIA += newAcceleration;
	}

	public bool getIsEnemy()
	{
		return isEnemy;
	}

	public void setShooting(bool _shooting)
	{
		this.shooting = _shooting;
	}

	public bool getShooting()
	{
		return this.shooting;
	}
}
