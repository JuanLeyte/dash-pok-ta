﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
public Transform waypoint;

private GameObject player;      
private Vector3 offset;
private Vector3 waypointOffset;
private Vector3 startingPos;

private Quaternion waypointPlane;
private Quaternion defaultRotation;

private bool changing; //Flag that avoids that goToNewTarget function and the update following doesn't run simultaneously...
private bool turning;

private float originalHeight;
private float dinamicHeight;

private float cameraY;
private float originalCameraY;

Vector3 angle;
Vector3 referenceOffset;

/*float offsetX;
float offsetY;
float offsetZ;*/

	void Start()
	{
		originalHeight = this.transform.position.y;
		dinamicHeight = 48f;
		turning = false;
		defaultRotation = this.transform.rotation;
		angle = transform.eulerAngles;
		this.changing = false;
		startingPos = this.transform.position;
		offset = startingPos - player.transform.position;
		waypointOffset = waypoint.position - player.transform.position;
		referenceOffset = offset;
		cameraY = transform.position.y;
		originalCameraY = cameraY;
		/*offsetX = startingPos.x - player.transform.position.x;
		offsetY = startingPos.y - player.transform.position.y;
		offsetZ = startingPos.z - player.transform.position.z;*/
	}

	void resetOffset()
	{
		offset = this.transform.position - player.transform.position;
	}

	/*public void changeTarget()
	{
		offset = transform.position - this.player.transform.position;
	}*/

	public void followTarget()
	{
		//float tempY = transform.position.y;
		transform.position = this.player.transform.position + offset;

		Vector3 tempPos = new Vector3(transform.position.x, cameraY, transform.position.z);
		transform.position = tempPos;
	}

	public void goToNewTarget()
	{
		this.changing = true;
		Vector3 goTo = this.player.transform.position + offset;
		this.transform.position = Vector3.Lerp(this.transform.position, goTo, 0.5f);
		this.changing = false;
	}



	public void setTarget(GameObject _player)
	{
		this.player = _player;
	}

	public GameObject getTarget()
	{
		return this.player;
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "referencePlane")
		{
			Debug.Log("Collided with Waypoint Plane");		
			
			//if(!turning)
			{
				Transform tempWaypoint = col.gameObject.GetComponent<WaypointPlane>().getWaypointPlane();
				Debug.Log("Waypoint Y: " + tempWaypoint.position.y);
				turning = true;
				waypointPlane = tempWaypoint.rotation; //Get the quaternion rotation of the reference plane you are colliding with...
				StartCoroutine(rotateCamera(tempWaypoint));
				//StopCoroutine(returnOriginalRotation());
			}	
		}

		if(col.gameObject.tag == "returnPlane")
		{
			//if(!turning)
			{
				turning = true;
				StartCoroutine(returnOriginalRotation());
				//StopCoroutine("rotateCamera");
			}		
		}
	}

	/*void OnTriggerStay(Collider col)
	{
		if(col.gameObject.tag == "referencePlane")
		{
			turning = true;
		}
	}*/

	/*void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag == "waypointPlane")
		{
			Debug.Log("Exited Collision with Waypoint Plane");
			StartCoroutine(returnOriginalRotation());

			if(turning)
			{
				turning = false;
			}
		}
	}*/

	IEnumerator rotateCamera(Transform _waypoint)
	{
		float rotationSpeed = 0.5f;
		Vector3 newPos = new Vector3(transform.position.x, _waypoint.position.y, _waypoint.position.z);

		float normalizedTime = 0.0f;
      	while (normalizedTime < 1.0f)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, waypointPlane, normalizedTime);
			transform.position = Vector3.Lerp(transform.position, newPos, normalizedTime);
			offset = Vector3.Lerp(offset, waypointOffset, normalizedTime);
			cameraY = Mathf.Lerp(cameraY, _waypoint.position.y, normalizedTime);

			normalizedTime += Time.deltaTime / rotationSpeed;
			
			resetOffset();

			yield return null;
		}

		turning = false;
		StopCoroutine(rotateCamera(_waypoint));
	}

	IEnumerator returnOriginalRotation()
	{
		float rotationSpeed = 0.5f;
		Vector3 newPos = new Vector3(transform.position.x, originalHeight, transform.position.z);

		float normalizedTime = 0.0f;
      	while (normalizedTime < 1.0f)
		{
			transform.rotation = Quaternion.Lerp(transform.rotation, defaultRotation, normalizedTime);
			transform.position = Vector3.Lerp(transform.position, newPos, normalizedTime);
			offset = Vector3.Lerp(offset, referenceOffset, normalizedTime);
			cameraY = Mathf.Lerp(cameraY, originalCameraY, normalizedTime);

			normalizedTime += Time.deltaTime / rotationSpeed;

			//resetOffset();

			yield return null;
		}

		turning = false;
		StopCoroutine(returnOriginalRotation());
	}

	IEnumerator goToNewTargetLerp()
	{
		this.changing = true;
		Vector3 goTo = this.player.transform.position + offset;

		float normalizedTime = 0.0f;
		while(normalizedTime < 1.0f)
		{
			this.transform.position = Vector3.Lerp(this.transform.position, goTo, normalizedTime);
			normalizedTime += Time.deltaTime / 0.5f;
			//resetOffset();

			yield return null;
		}
		
		this.changing = false;
		StopCoroutine(goToNewTargetLerp());
	}
	
	void Update()
	{
		//if(!changing)
		{
			followTarget();
		}
	}
}