﻿using UnityEngine;
using System.Collections;

public class BallChaser : MonoBehaviour
{
	private bool touchedBall;
	private float speed;
	public GameObject ball;

	public Material neutral;
	public Material touch;
	public Material freeze;

	private Renderer rend;

	// Use this for initialization
	void Start ()
	{
		touchedBall = false;
		speed = 5f;
		rend = this.GetComponent<Renderer>();
		rend.material = neutral;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		float tempY = this.transform.position.y;
		float tempBallX = ball.transform.position.x;
		float tempBallZ = ball.transform.position.z;

		Vector3 tempV = new Vector3(tempBallX, tempY, tempBallZ);

		//this.transform.position = Vector3.Lerp(this.transform.position, tempV, Time.deltaTime * speed);

		float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, tempV, step);
	}

	public bool getTouchedBall()
	{
		return this.touchedBall;
	}

	public void stop()
	{
		this.speed = 0f;
		this.rend.material = freeze;
	}

	void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Ball")
		{
			if(speed != 0)
			{
				touchedBall = true;
				rend.material = touch;
			}
			
		}
	}
}
