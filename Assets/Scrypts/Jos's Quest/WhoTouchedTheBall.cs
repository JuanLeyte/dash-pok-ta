﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WhoTouchedTheBall : MonoBehaviour
{
	public List<GameObject> players = new List<GameObject>();
	private List<BallChaser> bc;

	bool stop;

	// Use this for initialization
	void Start ()
	{
		stop = false;
		bc = new List<BallChaser>(players.Count);

		for(int i = 0; i < players.Count; i++)
		{
			bc.Insert(i, players[i].GetComponent<BallChaser>());
		}
	}

	void freezeOtherPlayers(int index)
	{
		for(int i = 0; i< players.Count; i++)
		{
			if(i != index)
			{
				bc[i].stop();
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(!stop)
		{
			for(int i = 0; i < players.Count; i++)
			{
				if(bc[i].getTouchedBall())
				{
					freezeOtherPlayers(i);
					stop = true;
				}
			}
		}
		
	}
}
