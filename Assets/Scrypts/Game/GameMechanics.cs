﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMechanics : MonoBehaviour
{
	private int times;
	public GameObject ball;

	private int playerScore;
	private int aiScore;

	[SerializeField] int minutes;
	[SerializeField] int seconds;

	private Timer timer;

	public Text playerScoreText;
	public Text aiScoreText;
	public Text retroText;

	public GameObject gameOverCanvasButton;

	public Text countdownText;

	public GameObject gameOverCanvas;

	private PlayerTeamManager ptm;
	private EnemyTeamManager etm;

	private SelectedPlayerManager spm;

	private bool gameOn;

	// Use this for initialization
	void Start()
	{
		times = 2;

		gameOn = false;
		countdownText.text = "Ready?";
		ptm = this.GetComponent<PlayerTeamManager>();
		etm = this.GetComponent<EnemyTeamManager>();
		spm = this.GetComponent<SelectedPlayerManager>();

		gameOverCanvas.SetActive(false);

		playerScore = 0;
		aiScore = 0;

		StartCoroutine(startRegressiveCount());

		//Stop the players...
		ptm.setTeamState("Unasigned");
		etm.setTeamState("Unasigned");

		timer = this.GetComponent<Timer>();
	}

	void Update()
	{
		if(gameOn)
		{
			timer.tick();
		}
	}

	public void repositionPlayersAndBall()
	{
		ptm.setTeamState("Unasigned");
		etm.setTeamState("Unasigned");

		Time.timeScale = 1.0f;

		//gameOn = false;
		timer.setInitialTimeValues();
		ptm.setOnInitialPositions();
		etm.setOnInitialPositions();
		ball.GetComponent<PlayersRayCast>().setInitialPosition();

		gameOverCanvas.SetActive(false);

		StartCoroutine(startRegressiveCount());
	}

	public void addScore(string _player)
	{
		switch(_player)
		{
			case "Player":
			aiScore++;
			aiScoreText.text = "AI: " + aiScore;
			break;

			case "AI":			
			playerScore++;
			playerScoreText.text = "Player: " + playerScore;
			break;
		}
	}

	public void resetGame()
	{
		SceneManager.LoadScene("gameplayScene",LoadSceneMode.Additive);
	}

	public void timeUp()
	{
		gameOn = false;

		gameOverCanvas.SetActive(true);
		Time.timeScale = 0.0f;
		times--;

		Debug.Log("Time: " +times);

		switch(times)
		{
			case 1:
			gameOverCanvasButton.transform.GetChild(0).GetComponent<Text>().text = "To second Half";

			if(playerScore > aiScore)
			{
				retroText.text = "You are Winning";
			}

			if(aiScore > playerScore)
			{
				retroText.text = "You are Losing";
			}

			if(playerScore == aiScore)
			{
				retroText.text = "You are Tied";
			}
			break;

			case 0:
			gameOverCanvasButton.transform.GetChild(0).GetComponent<Text>().text = "To Menu";

			if(playerScore > aiScore)
			{
				SceneManager.LoadScene("Post Juego");
				//retroText.text = "You Won!";
			}

			if(aiScore > playerScore)
			{
				SceneManager.LoadScene("Post Juego");
				//retroText.text = "You Lost!";
			}

			if(playerScore == aiScore)
			{
				SceneManager.LoadScene("Post Juego");
				//retroText.text = "Tie!";
			}
			break;
		}
	}

	public int getTime()
	{
		return this.times;
	}

	public int getSeconds()
	{
		return this.seconds;
	}

	public int getMinutes()
	{
		return this.minutes;
	}

	public bool getGameOn()
	{
		return this.gameOn;
	}

	public GameObject getBall()
	{
		return ball;
	}

	IEnumerator startRegressiveCount()
	{
		yield return new WaitForSeconds(2f);
		countdownText.text = "Go!";
		StartCoroutine(endRegressiveCount());

		StopCoroutine(startRegressiveCount());
	}

	IEnumerator endRegressiveCount()
	{
		yield return new WaitForSeconds(1f);
		countdownText.text = "";
		//ptm.setActivateMovement(true);
		/*spm.switchPlayer();
		etm.setActivateMovement(true);*/
		gameOn = true;
		ptm.setTeamState("Idle");
		etm.setTeamState("Idle");

		StopCoroutine(endRegressiveCount());

		//timer.tick();
	}
}
