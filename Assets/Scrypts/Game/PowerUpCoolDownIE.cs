﻿ using UnityEngine;
 using System;
 using System.Collections;

public class PowerUpCoolDownIE : MonoBehaviour 
{

	public GameObject ParticleChilli;
	public GameObject ParticleMooshroom;
	public GameObject ParticleClock;

	 GameObject Alien1;
	 GameObject Alien2;
     GameObject Alien3;

	 GameObject Player1;
	 GameObject Player2;
	 GameObject Player3;

	 GameObject goalPlayer;
	 GameObject goalAlien;

     GameObject collisionParticles;

	 Color colorPlayer;
	 Color colorAlien;

     GameObject trail1p;
     GameObject trail2p;
     GameObject trail3p;

     GameObject particles1;
     GameObject particles2;
     GameObject particles3;
     GameObject particlesAlien1;
     GameObject particlesAlien2;
     GameObject particlesAlien3;
	
     PlayersStats scryptAlien1;
	 PlayersStats scryptAlien2;
	 PlayersStats scryptAlien3;

	 PlayersStats scryptPlayer1;
	 PlayersStats scryptPlayer2;
	 PlayersStats scryptPlayer3;	


	 Renderer player;
	 Renderer alien;


	void Start () 
	{
		

		Player1 = GameObject.Find("Teotihuacan");
		Player2 = GameObject.Find("Teotihuacan 2");
		Player3 = GameObject.Find("Teotihuacan 3");
		Alien1 = GameObject.Find("Alien 1");
		Alien2 = GameObject.Find("Alien 2");
		Alien3 = GameObject.Find("Alien 3");

		trail1p = Player1.transform.Find("Trail").gameObject;
		trail2p = Player2.transform.Find("Trail").gameObject;
		trail3p = Player3.transform.Find("Trail").gameObject;

		goalPlayer = GameObject.FindWithTag("goalplayer");
		goalAlien = GameObject.FindWithTag("goalalien");

		scryptPlayer1 = Player1.GetComponent<PlayersStats>();
		scryptPlayer2 = Player2.GetComponent<PlayersStats>();
		scryptPlayer3 = Player3.GetComponent<PlayersStats>();

		particles1 = Player1.transform.Find("Particulas").gameObject;
 		particles2 = Player2.transform.Find("Particulas").gameObject;
 		particles3 = Player3.transform.Find("Particulas").gameObject;
 		particlesAlien1 = Alien1.transform.Find("Particulas").gameObject;
 		particlesAlien2 = Alien2.transform.Find("Particulas").gameObject;
 		particlesAlien3 = Alien3.transform.Find("Particulas").gameObject;
 		
	}

	public void coroutinePlayer(string name) 
	{
         StartCoroutine(PowerUpsCoolDownPlayer(name));
     }

     public void coroutineAlien(string name) 
     {
         StartCoroutine(PowerUpsCoolDownAlien(name));
     }

     public void coroutineHongo(string name) 
     {
         StartCoroutine(PowerUpsCoolDownHongo(name));
     }



     public void InstantiatePuParticle(string particleName,Vector3 posPU)
     {
     	if(particleName == "ParticleChilli")
     	{
     		GameObject tempPowerUpParticleObj = (GameObject) Instantiate(ParticleChilli, posPU, Quaternion.identity);

     		Destroy(tempPowerUpParticleObj,0.5f);
     	}
     	if (particleName == "ParticleMooshroom")
     	{
     		GameObject tempPowerUpParticleObj2 = (GameObject) Instantiate(ParticleMooshroom, posPU, Quaternion.identity);

     		Destroy(tempPowerUpParticleObj2,0.5f);
     	}
     	if (particleName == "ParticleClock")
     	{
     		GameObject tempPowerUpParticleObj3 = (GameObject) Instantiate(ParticleClock, posPU, Quaternion.identity);

     		Destroy(tempPowerUpParticleObj3,0.5f);
     	}
     }


	IEnumerator PowerUpsCoolDownPlayer(string name)
	{
		
		yield return new WaitForSeconds(3);
		
		if(name == "chile")
		{
				trail1p.SetActive(false);
				trail2p.SetActive(false);
				trail3p.SetActive(false);

				scryptPlayer1 = Player1.GetComponent<PlayersStats>();
				scryptPlayer2 = Player2.GetComponent<PlayersStats>();
				scryptPlayer3 = Player3.GetComponent<PlayersStats>();
				scryptPlayer1.setMaxSpeed(10);
		 		scryptPlayer2.setMaxSpeed(10);
		 		scryptPlayer3.setMaxSpeed(10);
		}

		else if(name == "reloj")
		{
			EfectoRelogN();
				 

		}
		
	}

	public void EfectoRelogN()
	{
		particlesAlien1.SetActive(false);
	 	particlesAlien2.SetActive(false);
	 	particlesAlien3.SetActive(false);
		scryptAlien1 = Alien1.GetComponent<PlayersStats>();
		scryptAlien2 = Alien2.GetComponent<PlayersStats>();	
		scryptAlien3 = Alien3.GetComponent<PlayersStats>();	
		scryptAlien1.setMaxSpeedIA(10);
	 	scryptAlien2.setMaxSpeedIA(10);
	 	scryptAlien3.setMaxSpeedIA(10);
	
	 	scryptAlien1.setSpeedIA(0.5f);
	 	scryptAlien2.setSpeedIA(0.5f);
	 	scryptAlien3.setSpeedIA(0.5f);
	}


	IEnumerator PowerUpsCoolDownAlien(string name)
	{
		
		yield return new WaitForSeconds(3.5f);
		if(name == "chile")
		{
				scryptAlien1.setMaxSpeedIA(10);
		 		scryptAlien2.setMaxSpeedIA(10);
		 		scryptAlien3.setMaxSpeedIA(10);

		 		scryptAlien1.setSpeedIA(0.5f);
		 		scryptAlien2.setSpeedIA(0.5f);
		 		scryptAlien3.setSpeedIA(0.5f);

		 		Debug.Log("CHILE ALIEN A LA NORMALIDAD");
	 			Debug.Log("CHILE ALIEN A LA NORMALIDAD");
	 			Debug.Log("CHILE ALIEN A LA NORMALIDAD");

		}

		if(name == "reloj")
		{
				scryptPlayer1.setMaxSpeed(15);
		 		scryptPlayer2.setMaxSpeed(15);
		 		scryptPlayer3.setMaxSpeed(15);
		 		
		 		scryptPlayer1.setAccelerationZ(1.0f);
		 		scryptPlayer2.setAccelerationZ(1.0f);
		 		scryptPlayer3.setAccelerationZ(1.0f);

		 		scryptPlayer1.setAccelerationX(1.0f);
		 		scryptPlayer2.setAccelerationX(1.0f);
		 		scryptPlayer3.setAccelerationX(1.0f);
		 		Debug.Log("RELOJ ALIEN A LA NORMALIDADyehhhhh");
	 			Debug.Log("RELOJ ALIEN A LA NORMALIDADyeahhhh");
	 			Debug.Log("RELOJ ALIEN A LA NORMALIDADyeahhhh");
		}
		
	}


	IEnumerator PowerUpsCoolDownHongo(string name)
		{

			yield return new WaitForSeconds(3);

			GameObject particles1 = Player1.transform.Find("Particulas").gameObject;
 			GameObject particles2 = Player2.transform.Find("Particulas").gameObject;
 			GameObject particles3 = Player3.transform.Find("Particulas").gameObject;
			particles1.SetActive(false);
			particles2.SetActive(false);
			particles3.SetActive(false);

		}






}
