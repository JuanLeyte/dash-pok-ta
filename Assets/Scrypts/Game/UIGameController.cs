﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIGameController : MonoBehaviour
{
	public List<GameObject> enemyTexts = new List<GameObject>();
	public List<GameObject> playerTexts = new List<GameObject>();

	private List<RectTransform> enemyRects;
	private List<RectTransform> playerRects;

	private List<Text> enemyTextsValues;
	private List<Text> playerTextsValues;

	private EnemyTeamManager etm;
	private PlayerTeamManager ptm;

	private List<GameObject> enemyTeam;
	private List<GameObject> playerTeam;

	public RectTransform canvasRectT;
	public bool showLevelsText = false;

	public GameObject minimap;

	private bool enabledTexts;
	private bool enabledMinimap;

	//public Camera mainCamera;

	void Awake()
	{
		etm = this.GetComponent<EnemyTeamManager>();
		ptm = this.GetComponent<PlayerTeamManager>();

		enabledTexts = false;
		enabledMinimap = true;

		enemyTeam = etm.getEnemyTeam();
		playerTeam = ptm.getPlayerTeam();

		enemyRects = new List<RectTransform>(enemyTeam.Count);
		playerRects = new List<RectTransform>(playerTeam.Count);
		enemyTextsValues = new List<Text>(enemyTeam.Count);
		playerTextsValues = new List<Text>(playerTeam.Count);

		for(int i = 0; i < enemyTexts.Count; i++)
		{
			enemyRects.Insert(i, enemyTexts[i].GetComponent<RectTransform>());
			enemyRects[i].transform.position = enemyTeam[i].transform.position;
			enemyRects[i].transform.position = new Vector3(enemyRects[i].transform.position.x, 10f, enemyRects[i].transform.position.z);
			enemyRects[i].transform.parent = enemyTeam[i].transform;

			enemyTextsValues.Insert(i, enemyTexts[i].transform.GetChild(0).GetComponent<Text>());
			enemyTextsValues[i].text = enemyTeam[i].name;
		}

		for(int i = 0; i < playerTexts.Count; i++)
		{
			playerRects.Insert(i, playerTexts[i].GetComponent<RectTransform>());
			playerRects[i].transform.position = playerTeam[i].transform.position;
			playerRects[i].transform.position = new Vector3(playerRects[i].transform.position.x, 10f, playerRects[i].transform.position.z);
			playerRects[i].transform.parent = playerTeam[i].transform;

			playerTextsValues.Insert(i, playerTexts[i].transform.GetChild(0).GetComponent<Text>());
			playerTextsValues[i].text = playerTeam[i].name;
		}
	}

	public void updateStatesText(string _teamCase, string _state)
	{
		switch(_teamCase)
		{
			case "Enemy":
			for(int i = 0; i < enemyTexts.Count; i++)
			{
				enemyTextsValues[i].text = _state;
			}
			break;

			case "Player":
			for(int i = 0; i < playerTexts.Count; i++)
			{
				playerTextsValues[i].text = _state;
			}
			break;

			default:
			break;
		}
	}

	public void updateLevelText(int _index, string _teamCase, int _level)
	{
		AIStateMachine tempAISM = null;

		switch(_teamCase)
		{
			case "Enemy":
			tempAISM = etm.getEnemyTeamMemeber(_index).GetComponent<AIStateMachine>();
			enemyTextsValues[_index].text = tempAISM.getCurrentState() + " " + _level;
			break;

			case "Player":
			tempAISM =  ptm.getPlayerTeamMember(_index).GetComponent<AIStateMachine>();
			playerTextsValues[_index].text = tempAISM.getCurrentState() + " " + _level;
			break;

			default:
			break;
		}
	}
	
	void Update()
	{
		if(showLevelsText)
		{
			if(!enabledTexts)
			{
				for(int i = 0; i < enemyRects.Count; i++)
				{
					enemyTexts[i].SetActive(true);
					playerTexts[i].SetActive(true);

					enabledTexts = true;
				}
			}

			if(enabledMinimap)
			{
				minimap.SetActive(false);
				enabledMinimap = false;
			}

			for(int i = 0; i < enemyRects.Count; i++)
			{
				/*Vector3 tempEnemyPos = etm.getEnemyTeamMemeber(i).transform.position;
				Vector3 tempPlayerPos = ptm.getPlayerTeamMember(i).transform.position;

				Vector2 enemyScreenPoint = RectTransformUtility.WorldToScreenPoint(mainCamera, tempEnemyPos);
				Vector2 playerScreenPoint = RectTransformUtility.WorldToScreenPoint(mainCamera, tempPlayerPos);

				enemyRects[i].anchoredPosition = enemyScreenPoint - canvasRectT.sizeDelta / 2f;
				playerRects[i].anchoredPosition = playerScreenPoint - canvasRectT.sizeDelta / 2f;*/

				enemyRects[i].transform.rotation = Quaternion.Euler(0f, 180f, 0f);
				playerRects[i].transform.rotation = Quaternion.Euler(0f, 180f, 0f);
			}
		}
		
		else if(!showLevelsText)
		{
			if(enabledTexts)
			{
				for(int i = 0; i < enemyRects.Count; i++)
				{
					enemyTexts[i].SetActive(false);
					playerTexts[i].SetActive(false);

					enabledTexts = false;
				}
			}

			if(!enabledMinimap)
			{
				minimap.SetActive(true);
				enabledMinimap = true;
			}
		}
	}
}
