﻿using UnityEngine;
using System.Collections;

public class PowerUpOnCollision : MonoBehaviour {

	private GameObject Alien1;
	private GameObject Alien2;
	private GameObject Alien3;

	private GameObject Player1;
	private GameObject Player2;
	private GameObject Player3;

	GameObject goalPlayer;
	GameObject goalAlien;

	GameObject collisionParticles;

	Color colorPlayer;
	Color colorAlien;

	private GameObject trail1p;
	private GameObject trail2p;
	private GameObject trail3p;

	 GameObject particles1;
	 GameObject particles2;
	 GameObject particles3;
	 GameObject particlesAlien1;
	 GameObject particlesAlien2;
	 GameObject particlesAlien3;

	private PowerUpCoolDownIE scryptCoolDown;
	private GameObject powerUpController;


	
	 PlayersStats scryptAlien1;
	 PlayersStats scryptAlien2;
	 PlayersStats scryptAlien3;

	 PlayersStats scryptPlayer1;
	 PlayersStats scryptPlayer2;
	 PlayersStats scryptPlayer3;


	Renderer player;
	Renderer alien;

	// Use this for initialization
	void Start () 
	{

		powerUpController = GameObject.Find("PowerUpmanager");
		scryptCoolDown = powerUpController.GetComponent<PowerUpCoolDownIE>();

		Player1 = GameObject.Find("Teotihuacan");
		Player2 = GameObject.Find("Teotihuacan 2");
		Player3 = GameObject.Find("Teotihuacan 3");

		Alien1 = GameObject.Find("Alien 1");
		Alien2 = GameObject.Find("Alien 2");
		Alien3 = GameObject.Find("Alien 3");

		trail1p = Player1.transform.Find("Trail").gameObject;
		trail2p = Player2.transform.Find("Trail").gameObject;
		trail3p = Player3.transform.Find("Trail").gameObject;

		goalPlayer = GameObject.FindWithTag("goalplayer");
		goalAlien = GameObject.FindWithTag("goalalien");

		particles1 = Player1.transform.Find("Particulas").gameObject;
 		particles2 = Player2.transform.Find("Particulas").gameObject;
 		particles3 = Player3.transform.Find("Particulas").gameObject;
 		particlesAlien1 = Alien1.transform.Find("Particulas").gameObject;
 		particlesAlien2 = Alien2.transform.Find("Particulas").gameObject;
 		particlesAlien3 = Alien3.transform.Find("Particulas").gameObject;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}


	 public void OnTriggerEnter(Collider other) 
	 {
		

	 	if (other.name == "Teotihuacan" || other.name == "Teotihuacan 2" || other.name == "Teotihuacan 3")
	 	{


	 		Debug.Log("Equipo Humano tomo el Power Up");

	 		if(this.tag == "chile")
	 		{
	 			/*collisionParticles = transform.Find("colision").gameObject;
	 			collisionParticles.SetActive(true);*/
	 			EfectoChilePlayer();
	 			
	 		}
	 		else if(this.tag == "reloj")
	 		{
	 			/*collisionParticles = transform.Find("colision").gameObject;
	 			collisionParticles.SetActive(true);*/
	 			EfectoRelojPlayer();
	 		}

	 		else if(this.tag == "hongo")
	 		{
	 		
	 			/*collisionParticles = transform.Find("colision").gameObject;
	 			collisionParticles.SetActive(true);*/
	 			EfectoHongoPlayer();
	 			
	 		}



	 	} 
	 	else if(other.name == "Alien 1" || other.name == "Alien 2" || other.name == "Alien 3")
	 	{
	 		
	 		

	 		Debug.Log("Equipo Alien tomo el Power Up");

	 		if(this.tag == "chile")
	 		{
				/*collisionParticles = transform.Find("colision").gameObject;
	 			collisionParticles.SetActive(true);*/
	 			EfectoChileAlien();
	 		}

	 		if(this.tag == "reloj")
	 		{
	 		/**/	/*collisionParticles = transform.Find("colision").gameObject;
	 			collisionParticles.SetActive(true);*/
	 			EfectoRelojAlien();
	 		}

	 		if(this.tag == "hongo")
	 		{
 			
	 			/*collisionParticles = transform.Find("colision").gameObject;
	 			collisionParticles.SetActive(true);*/
	 			EfectoHongoAlien();

	 		}

	 	}
        
    }






    public void EfectoChilePlayer()
    {
    			Vector3 posPU = this.transform.position;
	 			scryptCoolDown.InstantiatePuParticle("ParticleChilli",posPU);
	 			trail1p.SetActive(true);
	 			trail2p.SetActive(true);
	 			trail3p.SetActive(true);

	 			string name = "chile";
	 			scryptPlayer1 = Player1.GetComponent<PlayersStats>();
				scryptPlayer2 = Player2.GetComponent<PlayersStats>();
				scryptPlayer3 = Player3.GetComponent<PlayersStats>();

		 		scryptPlayer1.setMaxSpeed(30);
		 		scryptPlayer2.setMaxSpeed(30);
		 		scryptPlayer3.setMaxSpeed(30);
		 		scryptCoolDown.coroutinePlayer(name);
		 		Destroy(gameObject);
    }

    public void EfectoRelojPlayer()
    {
    			Vector3 posPU = this.transform.position;
	 			scryptCoolDown.InstantiatePuParticle("ParticleClock",posPU);
	 			string name = "reloj";
	 			scryptAlien1 = Alien1.GetComponent<PlayersStats>();
				scryptAlien2 = Alien2.GetComponent<PlayersStats>();	
				scryptAlien3 = Alien3.GetComponent<PlayersStats>();
	 			scryptAlien1.setMaxSpeedIA(2);
	 			scryptAlien2.setMaxSpeedIA(2);
	 			scryptAlien3.setMaxSpeedIA(2);
	
	 			scryptAlien1.setSpeedIA(0.1f);
	 			scryptAlien2.setSpeedIA(0.1f);
	 			scryptAlien3.setSpeedIA(0.1f);
	 			particlesAlien1.SetActive(true);
	 			particlesAlien2.SetActive(true);
	 			particlesAlien3.SetActive(true);
	 			scryptCoolDown.coroutinePlayer(name);

	 			Destroy(gameObject);
    }

    public void EfectoHongoPlayer()
    {
    			Vector3 posPU = this.transform.position;
	 			scryptCoolDown.InstantiatePuParticle("ParticleMooshroom", posPU);
	 			string name = "hongoPlayer";
	 			PowerUpCoolDownIE scryptCoolDowntemp = powerUpController.GetComponent<PowerUpCoolDownIE>();
	 			scryptCoolDowntemp.coroutineHongo(name);
	 			Destroy(gameObject);
    }

    //----------------------------------------

    public void EfectoChileAlien()
    {
    			Vector3 posPU = this.transform.position;
	 			scryptCoolDown.InstantiatePuParticle("ParticleChilli", posPU);
	 			string name = this.tag;

	 			scryptAlien1 = Alien1.GetComponent<PlayersStats>();
				scryptAlien2 = Alien2.GetComponent<PlayersStats>();	
				scryptAlien3 = Alien3.GetComponent<PlayersStats>();
		 		scryptAlien1.setMaxSpeedIA(25);
		 		scryptAlien2.setMaxSpeedIA(25);
		 		scryptAlien3.setMaxSpeedIA(25);

		 		scryptAlien1.setSpeedIA(1.5f);
		 		scryptAlien2.setSpeedIA(1.5f);
		 		scryptAlien3.setSpeedIA(1.5f);
		 		scryptCoolDown.coroutineAlien(name);
		 		Destroy(gameObject);
    }

    public void EfectoRelojAlien()
    {
    			Vector3 posPU = this.transform.position;
	 			scryptCoolDown.InstantiatePuParticle("ParticleClock", posPU);
	 			string name = this.tag;

	 			scryptPlayer1 = Player1.GetComponent<PlayersStats>();
				scryptPlayer2 = Player2.GetComponent<PlayersStats>();
				scryptPlayer3 = Player3.GetComponent<PlayersStats>();
		 		scryptPlayer1.setMaxSpeed(5);
		 		scryptPlayer2.setMaxSpeed(5);
		 		scryptPlayer3.setMaxSpeed(5);
		 		particles1.SetActive(true);
	 			particles2.SetActive(true);
	 			particles3.SetActive(true);
/*		 		scryptPlayer1.setAccelerationZ(0.2f);
		 		scryptPlayer2.setAccelerationZ(0.2f);
		 		scryptPlayer3.setAccelerationZ(0.2f);

		 		scryptPlayer1.setAccelerationX(0.2f);
		 		scryptPlayer2.setAccelerationX(0.2f);
		 		scryptPlayer3.setAccelerationX(0.2f);*/
		 		scryptCoolDown.coroutineAlien(name);
		 		Destroy(gameObject);
    }

    public void EfectoHongoAlien()
    {
    		 	Vector3 posPU = this.transform.position;
	 			scryptCoolDown.InstantiatePuParticle("ParticleMooshroom",posPU);
	 			string name = "hongoAlien";
				PowerUpCoolDownIE scryptCoolDowntemp = powerUpController.GetComponent<PowerUpCoolDownIE>();
	 			scryptCoolDowntemp.coroutineHongo(name);
	 			Destroy(gameObject);
    }





}
