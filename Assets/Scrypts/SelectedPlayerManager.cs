﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SelectedPlayerManager : MonoBehaviour
{
	public Camera mainCamera;
	public GameObject minimapMarker;
	public GameObject marker;
	private GameObject ball;
	public List<GameObject> players = new List<GameObject>(); //Stores each team player...

	private CameraFollow cf;
	private List<PlayerMove> pMoves; //Stores each PlayerMove script each team player is attached to...
	private List<PositionMovement> pMovements; //Stores each PositionMovement script attached to each team player...

	private int playerIndex; //Stores the index of the current selected players on the players list...

	void Start()
	{
		ball = this.GetComponent<GameMechanics>().getBall();
		//players = new List<GameObject>();
		pMoves = new List<PlayerMove>(players.Count);
		pMovements = new List<PositionMovement>(players.Count);

		cf = mainCamera.GetComponent<CameraFollow>();

		//Get all the team players scripts...
		for(int i = 0; i < players.Count; i++)
		{
			pMoves.Insert(i, players[i].GetComponent<PlayerMove>());
			pMovements.Insert(i, players[i].GetComponent<PositionMovement>());
		}

		playerIndex = 0; //Stores the first player added to the list. Beign the first that can be controlled...

		switchPlayer(calculateFarnessBetween()); //Starts switching to the first player on the list...
	}

	//Returns the index of the player closest to the ball when the user wants to switch player...
	int calculateFarnessBetween()
	{
		float[] playersDistances = new float [players.Count]; //Stores temporal distance calculations of all team players...
		//float sum = 0; //Since we need a value to compare, well pick a sure one, that makes that all the distances possible selection candidates...

		int tempBest = 0;
		int tempMedium;

		for(int i = 0; i < players.Count; i++)
		{
			//playersDistances[i] = Mathf.Sqrt(Mathf.Pow(Mathf.Abs(players[i].transform.position.x - ball.transform.position.x), 2) - Mathf.Pow(Mathf.Abs(players[i].transform.position.z - ball.transform.position.z), 2));
			playersDistances[i] = new Vector2((players[i].transform.position.x - ball.transform.position.x), players[i].transform.position.z - ball.transform.position.z).magnitude;
			//sum += playersDistances[i];

			//Debug.Log("Player " + (i+1) + " distance: " + playersDistances[i]);
		}

		//float tempLeast = sum; //Current champion. Which distance will defeat it...

		for(int i = 0; i < players.Count; i++)
		{
			if(playersDistances[i] == Mathf.Min(playersDistances[0], Mathf.Min(playersDistances[1], playersDistances[2])))
			{
				tempBest = i;
			}
		}

		//check if the least distance index, is already the index that references the selected player...
		if(tempBest == playerIndex)
		{
			for(int i = 0; i < players.Count; i++)
			{
				if(playersDistances[i] != Mathf.Max(playersDistances[0], Mathf.Max(playersDistances[1], playersDistances[2])) &&
				playersDistances[i] != Mathf.Min(playersDistances[0], Mathf.Min(playersDistances[1], playersDistances[2])))
				{
					tempMedium = i;
					tempBest = tempMedium;
				}
			}
		}

		//Debug.Log("Least value: " + playersDistances[playerIndex]);

		return playerIndex = tempBest;
	}

	public void switchPlayer(int _playerIndex)
	{
		for(int i = 0; i < players.Count; i++)
		{
			//Deactivate the AI movement script and activate the user control script...
			if(i == _playerIndex)
			{
				this.pMoves[i].enabled = true;
				this.pMovements[i].enabled = false;
			}
			else //Do the contrary...
			{
				AIStateMachine tempAISM = this.players[i].GetComponent<AIStateMachine>();

				this.pMoves[i].enabled = false;
				this.pMovements[i].enabled = true;
				//tempAISM.setCurrentState("Unasigned");
				//tempAISM.setStatus("Unasigned");
			}
		}

		updateCamera();
		reassignMarker();
	}

	void updateCamera() //Reassigns target parameters for the camare relative to the current player...
	{
		//cf.setTarget(players[playerIndex]);
		cf.setTarget(ball);
		//cf.goToNewTarget();
	}

	//Repositions the markers (The UI minimap, and the one above the current player), so it is on the current selected team player...
	void reassignMarker()
	{
		Vector3 tempMark = new Vector3(players[playerIndex].transform.position.x, 5f, players[playerIndex].transform.position.z);
		Vector3 tempMiniMap = new Vector3(players[playerIndex].transform.position.x, 7.5f, players[playerIndex].transform.position.z);

		marker.transform.parent = null;
		minimapMarker.transform.parent = null;
		marker.transform.position = tempMark;
		minimapMarker.transform.position = tempMiniMap;
		marker.transform.parent = players[playerIndex].transform;
		minimapMarker.transform.parent = players[playerIndex].transform;
	}

	void Update()
	{
		if(Input.GetKeyDown("z"))
		{
			Debug.Log("Switching Player...");
			switchPlayer(calculateFarnessBetween());
		}
	}


	///// Geters and Seters /////
	public int getPlayerIndex()
	{
		return this.playerIndex;
	}

	public void setPlayerIndex(int _playerIndex)
	{
		this.playerIndex = _playerIndex;
	}

	public float getPlayerDistance(int _index)
	{
		return new Vector2((players[_index].transform.position.x - ball.transform.position.x), players[_index].transform.position.z - ball.transform.position.z).magnitude;
	}
}
