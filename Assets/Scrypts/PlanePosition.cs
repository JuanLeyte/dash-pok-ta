﻿using UnityEngine;
using System.Collections;

public class PlanePosition : MonoBehaviour
{

	private float scale;
	private float moveAreaX;
	private float moveAreaZ;

	private Vector3 center;

	private Vector3 initialPos;

	// Use this for initialization
	void Awake ()
	{
		initialPos = this.transform.position;

		scale = 0.1f;
		moveAreaX = gameObject.GetComponent<Renderer>().bounds.size.x / 2;
		moveAreaZ = gameObject.GetComponent<Renderer>().bounds.size.z / 2;
		center = gameObject.GetComponent<Renderer>().bounds.center;

		Debug.Log("moveAreaX: " +moveAreaX);
		Debug.Log("moveAreaZ: " +moveAreaZ);
	}

	public Vector3 getInitialPos()
	{
		return this.initialPos;
	}

	public float getMoveAreaX()
	{
		return this.moveAreaX;
	}

	public float getMoveAreaZ()
	{
		return this.moveAreaZ;
	}

	public Vector3 getCenter()
	{
		return this.center;
	}

	
}
