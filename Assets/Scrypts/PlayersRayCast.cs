using UnityEngine;
using System.Collections;

public class PlayersRayCast : MonoBehaviour
{
	public GameObject game;
	public float time;
	private float offsetRaycast;

    Rigidbody rigidbody;
    Vector3[] identities;
    private Vector3 iniPos;
    public bool ShootWhitChanfle;

    bool vectorForward;
    bool vectorBack;
    bool vectorLeft;
    bool vectorRight;
    bool vectorUp;

    bool vectorForwardLeft;
    bool vectorForwardRight;
    bool vectorBackLeft;
    bool vectorBackRight;


    bool touched;
    private float angle;

    public Transform testGoal;

	void Start ()
	{
		ShootWhitChanfle = false;

		vectorRight = false;
		vectorLeft = false;
		vectorForward = false;
		vectorBack = false;
		vectorUp = false;

		vectorForwardLeft = false;
		vectorForwardRight = false;
		vectorBackLeft = false;
		vectorBackRight = false;
		iniPos = this.transform.position;




		touched = false;
		rigidbody = this.GetComponent<Rigidbody>();


		identities = new Vector3[]
		{
			new Vector3(2, 0, 0), //V3 Left
			new Vector3(-2, 0, 0), //V3 Right
			new Vector3(0, 0, 2), //V3  Forward
			new Vector3(0, 0, -2), //V3 Back

			new Vector3(0, 2, 0), //V3 Up

			new Vector3(2, 0, 2), //V3 Left For
			new Vector3(-2, 0, 2), //V3 Right For
			new Vector3(2, 0, -2), //V3 Left Ba
			new Vector3(-2, 0, -2) //V3 Right Ba
		};

		//StartCoroutine(countIfTouched());
	}

	void Update()
	{

		float velX;

		if(transform.position.y <= -5)
		{
			transform.position = iniPos;
			rigidbody.velocity = new Vector3(0,0,0);
		}

		if(rigidbody.velocity.y >= 10 )
		{
			rigidbody.velocity = new Vector3(rigidbody.velocity.x,10,rigidbody.velocity.z);

		}

		//Debug.Log("VEL PELOTA"+ rigidbody.velocity);
	}




	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Player")
		{
			//getPositionToHitTowardsTarget(testGoal);

			touched = true;
			StopCoroutine(countIfTouched());
			/*if(col.gameObject.GetComponent<AIBehavior>() != null)
			{
				col.gameObject.GetComponent<AIBehavior>().freezeAtacking();
			}*/
	
			if(col.gameObject.GetComponent<PlayersStats>().getIsEnemy()) //If collided with an enemy
			{
				game.GetComponent<EnemyTeamManager>().setTeamState("Atacking");
				game.GetComponent<PlayerTeamManager>().setTeamState("Defending");
			}
			else if(!col.gameObject.GetComponent<PlayersStats>().getIsEnemy())
			{
				game.GetComponent<EnemyTeamManager>().setTeamState("Defending");
				game.GetComponent<PlayerTeamManager>().setTeamState("Atacking");
			}

			RaycastHit hit;
			if(col.gameObject.name.Equals("Alien") || col.gameObject.name.Equals("Alien 2") ||  col.gameObject.name.Equals("Alien 3"))
			{
				offsetRaycast = 1.2f;
			}else if(col.gameObject.name.Equals("Teotihuacan") || col.gameObject.name.Equals("Teotihuacan 2") ||  col.gameObject.name.Equals("Teotihuacan 3"))
			{
				offsetRaycast = -0.5f;
			}
			Debug.Log("offsetRaycast: "+ offsetRaycast );
			Vector3 offset = new Vector3(0,offsetRaycast,0);
			float maxDistance = 2.0f;
			float force = 10f;

			//Globito
			if(!col.gameObject.GetComponent<PlayersStats>().getShooting())
			{
				force = 1.5f;
				vectorUp = true;
			}

			for(int i = 0; i < identities.Length; i++)
			{
				Ray tempRay = new Ray(col.transform.position + offset, col.transform.TransformDirection(identities[i]));
				Debug.DrawRay(col.transform.position+ offset, col.transform.TransformDirection(identities[i]), Color.blue, 2.0f);
				//Debug.Log(" " + identities[i]);
				if (Physics.Raycast(tempRay, out hit, maxDistance))
				{
					if(hit.collider != null)
					{
						Debug.DrawRay(col.transform.position+ offset, col.transform.TransformDirection(identities[i]),Color.red, 5.0f);
						//rigidbody.AddForce(col.transform.TransformDirection(identities[i]) * force,ForceMode.VelocityChange);
						setBooleanVectors(i);
					}	
				}

			}

		if(vectorForward == true)
		{
			rigidbody.AddForce(col.transform.TransformDirection(0,1,1) * force,ForceMode.VelocityChange);
		}
		if(vectorBack == true)
		{
			rigidbody.AddForce(col.transform.TransformDirection(0,0,-1) * force,ForceMode.VelocityChange);
		}
		if(vectorLeft == true)
		{
			rigidbody.AddForce(col.transform.TransformDirection(-1,0,0) * force,ForceMode.VelocityChange);
		}
		if(vectorRight == true)
		{
			rigidbody.AddForce(col.transform.TransformDirection(1,0,0) * force,ForceMode.VelocityChange);
		}
		if(vectorUp == true)
		{
			rigidbody.AddForce(col.transform.TransformDirection(0,-1.0f,2) * force,ForceMode.VelocityChange);
		}

		if(ShootWhitChanfle == true)
		{

				if(vectorForwardLeft == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);
				}
				if(vectorForwardRight == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);			
				}
				if(vectorBackLeft == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,-1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);
				}
				if(vectorBackRight == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,-1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);			
				}

				if(vectorForward == true && vectorForwardLeft == true || vectorForwardLeft == true && vectorLeft == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);
				}

				if(vectorForward == true && vectorForwardRight == true || vectorForwardRight == true && vectorRight == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);
				}

				if(vectorRight == true && vectorBackRight ==true ||vectorBackRight == true && vectorBack == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,-1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);
				}

				if(vectorLeft == true && vectorBackLeft == true || vectorBackLeft == true && vectorBack == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,-1+Random.Range(-1.0f, 1.0f)) * force,ForceMode.VelocityChange);
				}

		} else

		{

				if(vectorForwardLeft == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,1) * force,ForceMode.VelocityChange);
				}
				if(vectorForwardRight == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,1) * force,ForceMode.VelocityChange);			
				}
				if(vectorBackLeft == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,-1) * force,ForceMode.VelocityChange);
				}
				if(vectorBackRight == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,-1) * force,ForceMode.VelocityChange);			
				}
				
				if(vectorForward == true && vectorForwardLeft == true || vectorForwardLeft == true && vectorLeft == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,1) * force,ForceMode.VelocityChange);
				}

				if(vectorForward == true && vectorForwardRight == true || vectorForwardRight == true && vectorRight == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,1) * force,ForceMode.VelocityChange);
				}

				if(vectorRight == true && vectorBackRight ==true ||vectorBackRight == true && vectorBack == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(-1,0.2f,-1) * force,ForceMode.VelocityChange);
				}

				if(vectorLeft == true && vectorBackLeft == true || vectorBackLeft == true && vectorBack == true)
				{
					rigidbody.AddForce(col.transform.TransformDirection(1,0.2f,-1) * force,ForceMode.VelocityChange);
				}


		}

		setBooleanVectorsFalse();
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "playerGoal")
		{
			//Debug.Log("choco");
			game.GetComponent<GameMechanics>().addScore("Player");
		}
		if(col.gameObject.tag == "aiGoal")
		{
			//Debug.Log("choco");
			game.GetComponent<GameMechanics>().addScore("AI");
		}
	}

	void OnCollisionExit(Collision col)
	{
		if (col.gameObject.tag == "Player")
		{
			touched = false;
			StartCoroutine(countIfTouched());
		}
	}

	IEnumerator countIfTouched()
	{
		yield return new WaitForSeconds(time);

		if(!touched)
		{
			/*game.GetComponent<EnemyTeamManager>().setTeamState("Idle");
			game.GetComponent<PlayerTeamManager>().setTeamState("Idle");*/
		}

		StopCoroutine(countIfTouched());
	}

	public void setBooleanVectors(int i)
	{
		if (i == 0)
		{
			vectorLeft = true;
		}
		if (i == 1)
		{
			vectorRight = true;
		}
		if (i == 2)
		{
			vectorForward = true;
		}
		if (i == 3)
		{
			vectorBack = true;
		}
		if (i == 4)
		{
			vectorUp = true;
		}
		if (i == 5)
		{
			vectorForwardLeft = true;
		}
		if (i == 6)
		{
			vectorForwardRight = true;
		}
		if (i == 7)
		{
			vectorBackLeft = true;
		}
		if (i == 8)
		{
			vectorBackRight = true;
		}									
	}

	public void setBooleanVectorsFalse()
	{
		vectorLeft = false;
		vectorRight = false;
		vectorForward = false;
		vectorBack = false;
		vectorUp = false;
		vectorForwardLeft = false;
		vectorForwardRight = false;
		vectorBackLeft = false;
		vectorBackRight = false;									
	}

	public void setInitialPosition()
	{
		this.transform.position = iniPos;
	}
}
