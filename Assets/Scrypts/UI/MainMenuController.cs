﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class MainMenuController : MonoBehaviour 
{

	public void Jugar()
	{
		SceneManager.LoadScene("Pre Juego", LoadSceneMode.Single);
	}

	public void Configuraciones()
	{
		SceneManager.LoadScene("Configuracion", LoadSceneMode.Single);
	}

	public void Creditos()
	{
		SceneManager.LoadScene("Creditos",LoadSceneMode.Single);
	}
}
