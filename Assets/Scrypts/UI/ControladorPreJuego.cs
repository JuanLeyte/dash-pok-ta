﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControladorPreJuego : MonoBehaviour 
{
	public Button Atras;
	public Button Adelante;
	void Start()
	{
		Atras.interactable = false;
	}

	public void EquipoAnterior()
	{
		Adelante.interactable = true;
		Debug.Log("Equipo Teotihuacano activado");
		Atras.interactable = false;
	}

	public void EquipoSiguiente()
	{
		Atras.interactable = true;
		Debug.Log("Equipo Alien activado");
		Adelante.interactable = false;
	}

	public void ComenzarJuego()
	{
		SceneManager.LoadScene("gameplayScene 1", LoadSceneMode.Single);
	}

	public void Regresar()
	{
		SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
	}

}
