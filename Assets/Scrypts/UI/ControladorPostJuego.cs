﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ControladorPostJuego : MonoBehaviour 
{
	public void VolverAJugar()
	{
		SceneManager.LoadScene("gameplayScene 1", LoadSceneMode.Single);
	}
	public void SalirAlMenu()
	{
		SceneManager.LoadScene("Main Menu",LoadSceneMode.Single);
	}
}
