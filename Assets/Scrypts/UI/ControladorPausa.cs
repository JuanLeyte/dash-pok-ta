﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ControladorPausa : MonoBehaviour 
{
	public void RegresarMenu()
	{
		SceneManager.LoadScene("Main Menu",LoadSceneMode.Single);
	}
}
