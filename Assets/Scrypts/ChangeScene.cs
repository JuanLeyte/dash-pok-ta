﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {
public float timeLeft = 10.0f;
public float timeToWait = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	timeLeft -= Time.deltaTime;

	if(timeLeft <= timeToWait)
	{
		SceneManager.LoadScene("PressStartScene");
	}

	}
}
