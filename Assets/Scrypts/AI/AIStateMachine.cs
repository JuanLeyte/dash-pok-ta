﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIStateMachine : MonoBehaviour
{
	public GameObject game;

	public Material idle;
	public Material atacking;
	public Material defending;

	///// Enumerators //////
	private enum States { Unasigned, Idle, Atacking, Defending }; //All the posible states..
	private States currentState;
	private enum Levels { Unasigned, One, Two, Three };
	private Levels level;
	private enum Status{ Unasigned, Doing, Done, Failed };
	private Status status;

	//Possible to eliminate...
	private enum Paradigms { Offensive, Defensive };
	private Paradigms paradigm;

	private Renderer rend; //Temporal variable for states checking...

	private bool requestingState; //Flag to avoid multiple state requestings...
	private float normalizedScale;

	void Awake ()
	{
		//Debug.Log(getCurrentState());

		currentState = States.Unasigned;
		status = Status.Unasigned;
		level = Levels.Unasigned;
		paradigm = Paradigms.Offensive;
		
		rend = this.GetComponent<Renderer>();

		normalizedScale = 1.0f;
	}

	///// States Requests /////

	public void setLevelMaterial(string _level)
	{
		Renderer tempRend = this.GetComponent<Renderer>();
		//float tempNormalizedScale = 1.0f;

		switch(_level)
		{
			case "One":
			normalizedScale = 1.0f;
			break;

			case "Two":
			normalizedScale = 0.6f;
			break;

			case "Three":
			normalizedScale = 0.3f;
			break;
		}

		tempRend.material.color = changeAlpha(tempRend.material.color, normalizedScale);
	}

	Color changeAlpha(Color _color, float newAlpha)
	{
		_color.r = newAlpha;
		return _color;
	}

	/////  States Requests /////



	///// Geters and Seters /////

	public float getNormalizedScale()
	{
		return this.normalizedScale;
	}

	public string getCurrentState()
	{
		return this.currentState.ToString();
	}

	public string getCurrentStatus()
	{
		return this.status.ToString();
	}

	public string getCurrentParadigm()
	{
		return this.paradigm.ToString();
	}

	public string getCurrentLevel()
	{
		return this.level.ToString();
	}

	public void setCurrentState(string _state)
	{
		//if(stateRequest(_state))
		{
			switch(_state)
			{
				case "Unasigned":
				this.currentState = States.Unasigned;
				rend.material = idle;
				break;

				case "Idle":
				this.currentState = States.Idle;
				rend.material = idle;
				break;

				case "Atacking":
				this.currentState = States.Atacking;
				rend.material = atacking;
				break;

				case "Defending":
				this.currentState = States.Defending;
				rend.material = defending;
				break;

				default:
				this.currentState = this.currentState; //Does nothing actually...
				Debug.LogError("State " +_state+ " not defined in State enumerator.");
				break;
			}
		}

		/*else
		{
			this.currentState = States.Idle;
			rend.material = idle;
		}*/
		
	}

	public void setStatus(string status)
	{
		switch(status)
		{
			case "Unasigned":
			this.status = Status.Unasigned;
			break;

			case "Doing":
			this.status = Status.Doing;
			break;

			case "Done":
			this.status = Status.Done;
			break;

			case "Failed":
			this.status = Status.Failed;
			break;

			default:
			this.status = this.status; //Does nothing actually...
			Debug.LogError("Status " +status+ " not defined in Status enumerator.");
			break;
		}
	}

	public void setParadigm(string _paradigm)
	{
		switch(_paradigm)
		{
			case "Offensive":
			this.paradigm = Paradigms.Offensive;
			break;

			case "Defensive":
			this.paradigm = Paradigms.Defensive;
			break;

			default:
			this.paradigm = this.paradigm; //Does nothing actually...
			Debug.LogError("Paradigm " +_paradigm+ " not defined in Paradigm enumerator.");
			break;
		}
	}

	public void setLevel(string _level)
	{
		switch(_level)
		{
			case "One":
			this.level = Levels.One;
			break;

			case "Two":
			this.level = Levels.Two;
			break;

			case "Three":
			this.level = Levels.Three;
			break;

			default:
			this.paradigm = this.paradigm; //Does nothing actually...
			Debug.LogError("Level " +_level+ " not defined in Levels enumerator.");
			break;
		}

		setLevelMaterial(_level);
	}

	public GameObject getGame()
	{
		return this.game;
	}

	///// Geters and Seters /////
}
