﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class PositionMovement : MonoBehaviour
{
	public GameObject ball;
	//public GameObject positionZone;

	//Field Zones...
	/*public GameObject defendingPlane;
	public GameObject attackingPlane;
	public GameObject idlePlane;
	public GameObject liberalPlane;*/

	public Transform referencePlane;
	float planeDistance;

	public float dif = 0f;

	private PlayersStats playersStatsScrypts;

	private AIStateMachine aism; //To get both states and status...
	private bool goToGoalBool;
	private float normalizedScale;
	private bool onTargetRange; //Flag activated when the player is near the ball so it can hit it, and also stops it so it doesn't go literally where the ball is...

	private GameObject target;
	private Transform goal;

	private AIBehavior aib;

	private float distance;

	private bool playerExclusivity;

	private Transform referencePlayerPlaneName; //Stores the reference player plane, so it avoids re-checking...

	Vector3[] identities;

	////*Test For The Behavior Trees*////
	private EnemyTeamManager etm;
	private PlayerTeamManager ptm;
	//Participants geters...
	private List<GameObject> team; //Ally team...
	private List<GameObject> enemyTeam; //Rival team...
	private GameObject game;
	////*Test For The Behavior Trees*////

	//Reference Plane Checkers Handlers...
	string lastLevel;
	string actualLevel;


	////Testing the Turning /////
	private Quaternion _lookRotation;
    private Vector3 _direction;
    private float RotationSpeed = 3f;

	void Awake()
	{
		playerExclusivity = false;
		aib = this.GetComponent<AIBehavior>();
		aism = this.GetComponent<AIStateMachine>();
		normalizedScale = 1f;
		playersStatsScrypts = GetComponent<PlayersStats>();

		target = ball;

		referencePlayerPlaneName = null;

		identities = new Vector3[]
		{
			//Original Identities... Will define the orientation on the quadrant of the coordinates for hitting to target positioning...
			new Vector3(1, 0, 1),
			new Vector3(-1, 0, 1),
			new Vector3(-1, 0, -1),
			new Vector3(1, 0, -1)
		};

		referencePlane = null;
		planeDistance = 5f;

		////*Test*////
		///Who is your team and who is your enemy establishment...
		game = aism.getGame();

		if(playersStatsScrypts.getIsEnemy())
		{
			etm = game.GetComponent<EnemyTeamManager>();
			ptm = game.GetComponent<PlayerTeamManager>();
			team = etm.getEnemyTeam();
			//enemyTeam = ptm.getPlayerTeam();
		}
		else
		{
			ptm = game.GetComponent<PlayerTeamManager>();
			etm = game.GetComponent<EnemyTeamManager>();
			team = ptm.getPlayerTeam();
			//enemyTeam = etm.getEnemyTeam();
		}
		////*Test*////
	}

     float getPlaneDistance()
     {
     	float d =  new Vector2((this.transform.position.x - referencePlane.transform.position.x), this.transform.position.z - referencePlane.transform.position.z).magnitude;

		return d;
     }


	//Makes the player heads to the direction the ball is...
	public float headOnTarget(GameObject _target)
	{
		Vector3 playerPos = this.transform.position;
		Vector3 targetPos = _target.transform.position;

		float angle;
		float x = (playerPos.x - targetPos.x);
		float z = (playerPos.z - targetPos.z);

		if(playerPos.z < targetPos.z || playerPos.x < targetPos.x && playerPos.z < targetPos.z)
		{
			angle = (Mathf.Atan2(z, x) + (Mathf.PI * 2)) * Mathf.Rad2Deg;
		}
		else
		{
			angle = Mathf.Atan2(z, x) * Mathf.Rad2Deg;
		}

		return angle;
	}

	public void checkLevelForState(string _level, string _state)
	{
		if(_state == "Idle")
		{
			switch(_level)
			{
				case "One":
				//Avoid multiple value setups...
				if(target != ball) setTarget(ball);
				if(referencePlane != null) referencePlane = null;
				if(!playerExclusivity)
				{
					if(getDistance() >= 3.0f)
					{
						determineBestTargetToHit();
						aimFromBallToTarget();
					}
					else if(getDistance() >= 1.5f)
					{
						goToTarget(target);
					}
					
				}
				break;

				case "Two":
				//if(aism.getCurrentState() == "Atacking")
				//setReferencePlane(attackingPlane);
				//else
				//setReferencePlane(liberalPlane);
				break;

				case "Three":
				//if(aism.getCurrentState() == "Atacking")
				//setReferencePlane(defendingPlane);
				//else
				//setReferencePlane(idlePlane);
				break;
			}
		}

		else if(_state == "Defending")
		{
			switch(_level)
			{
				case "One":
				//Avoid multiple value setups...
				//if(target != ball) setTarget(ball);
				if(referencePlane != null) referencePlane = null;
				if(!playerExclusivity)
				{
					if(getDistance() >= 3.0f)
					{
						

						determineBestTargetToHit();
						aimFromBallToTarget();
					}
					else if(getDistance() >= 1.5f)
					goToTarget(target);
				}
				break;

				case "Two":
				//if(aism.getCurrentState() == "Atacking")
				//setReferencePlane(attackingPlane);
				//else
				//setReferencePlane(liberalPlane);

				//if(referencePlane == null)
				{
					//Debug.Log("Dont have referencePlane");
						if(!playersStatsScrypts.getIsEnemy())
						{
							//if(referencePlayerPlaneName == null)
							{
								//Debug.Log("Proceed to establish referencePlane");
								referencePlayerPlaneName = ptm.determinePlanePlayerIs(ptm.getPlayerWithLevelIndex("One")); //Pass the level 1 player...
								//setReferencePlane(ptm.getReferencePlane(referencePlayerPlaneName));
								setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));
							}
								

							//if(referencePlayerPlaneName != null)
								
						}
						else
						{
							referencePlayerPlaneName = etm.determinePlanePlayerIs(etm.getPlayerWithLevelIndex("One")); //Pass the level 1 player...
							//setReferencePlane(ptm.getReferencePlane(referencePlayerPlaneName));
							setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));

						}				
				}

				//else if(referencePlane != null)
				{
					/*if(transform.position != referencePlane.position)
					goToPlane(referencePlane);

					else
					{
						float dst = getPlaneDistance();

						if(dst > planeDistance)
						{
							Vector3 vect = referencePlane.transform.position - transform.position;
							vect = vect.normalized;
							vect *= (dst - planeDistance);
							//transform.position += vect;

							vect += transform.position;

							moveRelativeToPlane(vect);
						}
					}*/

					goBetweenPlaneAndTarget();
				}
				break;

				case "Three":
				//if(aism.getCurrentState() == "Atacking")
				//setReferencePlane(defendingPlane);
				//else
				//setReferencePlane(idlePlane);

				//if(referencePlane == null)
				{
					//Debug.Log("Dont have referencePlane");
						if(!playersStatsScrypts.getIsEnemy())
						{
							//if(referencePlayerPlaneName == null)
							{
								//Debug.Log("Proceed to establish referencePlane");
								referencePlayerPlaneName = ptm.determinePlanePlayerIs(ptm.getPlayerWithLevelIndex("Two")); //Pass the level 1 player...
								//setReferencePlane(ptm.getReferencePlane(referencePlayerPlaneName));
								setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));
							}
								

							//if(referencePlayerPlaneName != null)
								
						}

						else
						{
							referencePlayerPlaneName = etm.determinePlanePlayerIs(etm.getPlayerWithLevelIndex("Two")); //Pass the level 1 player...
							//setReferencePlane(ptm.getReferencePlane(referencePlayerPlaneName));
							setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));
						}					
				}

				//else if(referencePlane != null)
				{
					goBetweenPlaneAndTarget();
					
				}
				break;
			}
		}

		else if(_state == "Atacking")
		{
			switch(_level)
			{
				case "One":
				//Avoid multiple value setups...
				//if(target != ball) setTarget(ball);
				if(referencePlane != null) referencePlane = null;
				if(!playerExclusivity)
				{
					if(getDistance() >= 3.0f)
					{
						

						determineBestTargetToHit();
						aimFromBallToTarget();
					}
					else if(getDistance() >= 1.5f)
					{
						goToTarget(target);
					}
					
				}
				break;

				case "Two":

				//Plane Assignation
				//if(referencePlane == null)
				{
					//Debug.Log("Dont have referencePlane");
						if(!playersStatsScrypts.getIsEnemy())
						{
							//if(referencePlayerPlaneName == null)
							{
								//Debug.Log("Proceed to establish referencePlane");
								referencePlayerPlaneName = ptm.determinePlanePlayerIs(ptm.getPlayerWithLevelIndex("One")); //Pass the level 1 player...
								setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));
							}
								

							//if(referencePlayerPlaneName != null)
								
						}

						else
						{
							referencePlayerPlaneName = etm.determinePlanePlayerIs(etm.getPlayerWithLevelIndex("One")); //Pass the level 1 player...
							//setReferencePlane(ptm.getReferencePlane(referencePlayerPlaneName));
							setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));
						}				
				}

				//else if(referencePlane != null)
				{
					goBetweenPlaneAndTarget();
					
				}
					
				break;

				case "Three":
				//if(aism.getCurrentState() == "Atacking")
				//setReferencePlane(defendingPlane);
				//else
				//setReferencePlane(idlePlane);

				//if(referencePlane == null)
				{
					//Debug.Log("Dont have referencePlane");
						if(!playersStatsScrypts.getIsEnemy())
						{
							//if(referencePlayerPlaneName == null)
							{
								//Debug.Log("Proceed to establish referencePlane");
								referencePlayerPlaneName = ptm.determinePlanePlayerIs(ptm.getPlayerWithLevelIndex("Two")); //Pass the level 1 player...
								//setReferencePlane(ptm.getReferencePlane(referencePlayerPlaneName));
								setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));
							}
								

							//if(referencePlayerPlaneName != null)
								
						}

						else
						{
							referencePlayerPlaneName = etm.determinePlanePlayerIs(etm.getPlayerWithLevelIndex("Two")); //Pass the level 1 player...
							//setReferencePlane(ptm.getReferencePlane(referencePlayerPlaneName));
							setReferencePlane(getBestPlaneToPosition(referencePlayerPlaneName.name));
						}				
				}

				//else if(referencePlane != null)
				{
					goBetweenPlaneAndTarget();
					
				}
				break;
			}
		}
	}

	public Transform getBestPlaneToPosition(string _playerReferencePlane)
	{
		Transform tempPlane = null;

		//Debug.Log("Player Reference Plane: " +_playerReferencePlane);

		if(aism.getCurrentState() == "Atacking")
		{
			switch(_playerReferencePlane)
			{
				case "Plane_A":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("B");
				}
				else
				{
					tempPlane = etm.getPlane("D");
				}
				break;

				case "Plane_B":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("A");
				}
				else
				{
					tempPlane = etm.getPlane("C");
				}
				break;

				case "Plane_C":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("A");
				}
				else
				{
					tempPlane = etm.getPlane("B");
				}
				break;

				case "Plane_D":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("B");
				}
				else
				{
					tempPlane = etm.getPlane("A");
				}
				break;

				default:
				Debug.LogError("Plane name " +_playerReferencePlane+ " not within the plane names...");
				break;
			}
		}

		else if(aism.getCurrentState() == "Defending")
		{
			switch(_playerReferencePlane)
			{
				case "Plane_A":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("C");
				}
				else
				{
					tempPlane = etm.getPlane("B");
				}
				break;

				case "Plane_B":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("C");
				}
				else
				{
					tempPlane = etm.getPlane("B");
				}
				break;

				case "Plane_C":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("B");
				}
				else
				{
					tempPlane = etm.getPlane("C");
				}
				break;

				case "Plane_D":
				if(!playersStatsScrypts.getIsEnemy())
				{
					tempPlane = ptm.getPlane("B");
				}
				else
				{
					tempPlane = etm.getPlane("D");
				}
				break;

				default:
				Debug.LogError("Plane name " +_playerReferencePlane+ " not within the plane names...");
				break;
			}
		}
		

		return tempPlane;
	}

	//Follows the behavior tree that determines if a player should target the ball to hit goal, or a player so it passes it the ball...
	public void determineBestTargetToHit()
	{
		Transform tempPlane = null;

		if(!playersStatsScrypts.getIsEnemy()) 
		tempPlane = etm.determinePlanePlayerIs(ptm.getPlayerWithLevelIndex(aism.getCurrentLevel())); //Pass the level of the player...
		else
		tempPlane = ptm.determinePlanePlayerIs(etm.getPlayerWithLevelIndex(aism.getCurrentLevel())); //Pass the level of the player...

		switch(tempPlane.name)
		{
				case "Plane_A":
				if(!playersStatsScrypts.getIsEnemy())
				{
					if(ptm.countPlayersOnPlane("A") >= 2)
					{
						Transform referencePlayer = ptm.getPlayerTeamMember(ptm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);
						Debug.Log("Have more than 2 players on " +tempPlane.name+ ". Changed the target for a player");

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				else
				{
					if(etm.countPlayersOnPlane("C") >= 2)
					{
						Transform referencePlayer = etm.getEnemyTeamMemeber(etm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);
						Debug.Log("Have more than 2 players on " +tempPlane.name+ ". Changed the target for a player");

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				break;

				case "Plane_B":
				if(!playersStatsScrypts.getIsEnemy())
				{
					if(ptm.countPlayersOnPlane("A") >= 2)
					{
						Transform referencePlayer = ptm.getPlayerTeamMember(ptm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				else
				{
					if(etm.countPlayersOnPlane("D") >= 2)
					{
						Transform referencePlayer = etm.getEnemyTeamMemeber(etm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);
						Debug.Log("Have more than 2 players on " +tempPlane.name+ ". Changed the target for a player");

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				break;

				case "Plane_C":
				if(!playersStatsScrypts.getIsEnemy())
				{
					if(ptm.countPlayersOnPlane("A") >= 2)
					{
						Transform referencePlayer = ptm.getPlayerTeamMember(ptm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);
						Debug.Log("Have more than 2 players on " +tempPlane.name+ ". Changed the target for a player");

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				else
				{
					if(etm.countPlayersOnPlane("D") >= 2)
					{
						Transform referencePlayer = etm.getEnemyTeamMemeber(etm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);
						Debug.Log("Have more than 2 players on " +tempPlane.name+ ". Changed the target for a player");

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				break;

				case "Plane_D":
				if(!playersStatsScrypts.getIsEnemy())
				{
					if(ptm.countPlayersOnPlane("B") >= 2)
					{
						Transform referencePlayer = ptm.getPlayerTeamMember(ptm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);
						Debug.Log("Have more than 2 players on " +tempPlane.name+ ". Changed the target for a player");

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				else
				{
					if(etm.countPlayersOnPlane("D") >= 2)
					{
						Transform referencePlayer = etm.getEnemyTeamMemeber(etm.getPlayerWithLevelIndex("Two")).transform;
						setGoal(referencePlayer);
						Debug.Log("Have more than 2 players on " +tempPlane.name+ ". Changed the target for a player");

						playersStatsScrypts.setShooting(false);
					}
					else
					{
						setGoal(ball.transform);

						playersStatsScrypts.setShooting(true);
					}
				}
				break;

				default:
				Debug.LogError("Plane name " +tempPlane.name+ " not within the plane names...");
				break;
		}
	}
	
	void Update ()
	{
		/*actualLevel = this.aism.getCurrentLevel();

		//New referencePlanes checkers handlers...
		if(actualLevel != lastLevel)
		{
			referencePlayerPlaneName = null;
			referencePlane = null;
		}*/

		/*Quaternion rotation = Quaternion.Euler(0f, headOnTarget(target), 0f);
        transform.rotation = Quaternion.Lerp(this.transform.rotation, rotation, Time.deltaTime);*/

        //transform.LookAt(target.transform);


        //find the vector pointing from our position to the target
        _direction = (target.transform.position - transform.position).normalized;
        //_direction.x = 0f;
        //_direction.z = 0f;
        _direction.y = 0f;
 
         //create the rotation we need to be in to look at the target
        _lookRotation = Quaternion.LookRotation(_direction);
 
         //rotate us over time according to speed until we are in the required rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * RotationSpeed);

        //transform.eulerAngles = new Vector3(0f, headOnTarget(target), 0f);

        if(aism.getCurrentState() != "Unasigned") //Unasigned state avoids the players to move, used when the session restarts...
        {
	        checkLevelForState(aism.getCurrentLevel(), aism.getCurrentState());
	        //StartCoroutine(checkLevels());
        }

        //lastLevel = this.aism.getCurrentLevel();		
	}

	//NO!
	IEnumerator checkLevels()
	{
		yield return new WaitForSeconds(0.1f);
		checkLevelForState(aism.getCurrentLevel(), aism.getCurrentState());
	}

	///// Coroutines /////

	void goToTarget(GameObject _target)
	{
		//aism.setStatus("Doing");

		//if(aism.getCurrentStatus() != "Done")
		{
			if(playersStatsScrypts.getSpeedIA() >= playersStatsScrypts.getMaxSpeedIA())
			{
				playersStatsScrypts.setSpeedIA(playersStatsScrypts.getMaxSpeedIA());
			}
			playersStatsScrypts.accelerateIA(1f);
			Vector3 destination = new Vector3(_target.transform.position.x, this.transform.position.y, _target.transform.position.z);

			transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * playersStatsScrypts.getSpeedIA() * aism.getNormalizedScale());

			/*if(onTargetRange)
			{
				Debug.Log("On Ball Range");
				aism.setStatus("Done");
				aib.finishAtackig();
				aism.setCurrentState("Unasigned");
				//StopCoroutine(goToBall());
			}
			else
			{
				Debug.Log("Me not in plane...");
			}*/
		}

		//yield return null;	
	}

	void aimFromBallToTarget()
	{
		if(playersStatsScrypts.getSpeedIA() >= playersStatsScrypts.getMaxSpeedIA())
			{
				playersStatsScrypts.setSpeedIA(playersStatsScrypts.getMaxSpeedIA());
			}
			playersStatsScrypts.accelerateIA(1f); 
			Vector3 destination = getPositionToHitTowardsTarget(ball.transform, goal);

			transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * playersStatsScrypts.getSpeedIA() * aism.getNormalizedScale());
	}

	void goToPlane(Transform _target)
	{
		if(playersStatsScrypts.getSpeedIA() >= playersStatsScrypts.getMaxSpeedIA())
			{
				playersStatsScrypts.setSpeedIA(playersStatsScrypts.getMaxSpeedIA());
			}
			playersStatsScrypts.accelerateIA(1f);
			Vector3 destination = new Vector3(_target.position.x, this.transform.position.y, _target.position.z);

			transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * playersStatsScrypts.getSpeedIA() * aism.getNormalizedScale());
	}

	//It goes to the medium point between the target and the reference plane points...
	void goBetweenPlaneAndTarget()
	{
		float tempX = ((referencePlane.position.x + target.transform.position.x) / 2) * aism.getNormalizedScale();
		float tempZ = ((referencePlane.position.z + target.transform.position.z) / 2) * aism.getNormalizedScale();

		if(playersStatsScrypts.getSpeedIA() >= playersStatsScrypts.getMaxSpeedIA())
			{
				playersStatsScrypts.setSpeedIA(playersStatsScrypts.getMaxSpeedIA());
			}
			playersStatsScrypts.accelerateIA(1f);

		Vector3 destination = new Vector3(tempX, this.transform.position.y, tempZ);

		transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * playersStatsScrypts.getSpeedIA() * aism.getNormalizedScale());
	}

	void moveRelativeToPlane(Vector3 _pos)
	{
		if(playersStatsScrypts.getSpeedIA() >= playersStatsScrypts.getMaxSpeedIA())
			{
				playersStatsScrypts.setSpeedIA(playersStatsScrypts.getMaxSpeedIA());
			}
			playersStatsScrypts.accelerateIA(1f);
			//Vector3 destination = new Vector3(_pos.position.x, this.transform.position.y, _pos.position.z);

			transform.position = Vector3.MoveTowards(transform.position, _pos, Time.deltaTime * playersStatsScrypts.getSpeedIA() * aism.getNormalizedScale());
	}

	IEnumerator waitAndDetermine()
	{
		yield return new WaitForSeconds(0.45f);
		Debug.Log("Status is: " + aism.getCurrentStatus());

		StartCoroutine(waitAndDetermine());
		//calculateBallFarness();
	}

	///// Coroutines /////



	///// Geters and Seters /////

	public void setPlayerExclusivity(bool _playerExclusivity)
	{
		this.playerExclusivity = _playerExclusivity;
	}

	public void setGoal(Transform _goal)
	{
		this.goal = _goal;
	}

	public void setOnTargetRange(bool _onTargetRange)
	{
		this.onTargetRange = _onTargetRange;
	}

	public void setTarget(GameObject _target)
	{
		this.target = _target;
	}

	//Returns distance between the ball and the player...
	public float getDistance()
	{
		float d =  new Vector2((this.transform.position.x - target.transform.position.x), this.transform.position.z - target.transform.position.z).magnitude;

		return d;
	}

	public bool getPlayerExclusivity()
	{
		return this.playerExclusivity;
	}

	public GameObject getBall()
	{
		return this.ball;
	}

	public void setNormalizedScale(float _normalizedScale)
	{
		this.normalizedScale = _normalizedScale;
	}

	public void setReferencePlane(Transform _referencePlane)
	{
		this.referencePlane = _referencePlane;
	}

	///// Geters and Seters /////

	///// Hit to the Goal /////

	public float angleBetweenTarget(Transform _from, Transform _target)
    {
    	float angle = 0.0f;

    	Vector3 thisPos = _from.transform.position;
        Vector3 targetPos = _target.transform.position;
        float x = (thisPos.x - targetPos.x);
        float z = (thisPos.z - targetPos.z);

        if(thisPos.z < targetPos.z || thisPos.x < targetPos.x && thisPos.z < targetPos.z)
        {
            angle = (Mathf.Atan2(z, x) + (Mathf.PI * 2)) * Mathf.Rad2Deg;
        }
        else
        {
            angle = Mathf.Atan2(z, x) * Mathf.Rad2Deg;
        }

        return angle;
    }

    public Vector3 getPositionToHitTowardsTarget(Transform _from, Transform _target)
    {
    	Vector3 ballPos = new Vector3(_from.transform.position.x, this.transform.position.y, _from.transform.position.z);
    	float angle = angleBetweenTarget(_from, _target);

    	float tempX;
    	float tempZ;
    	Vector3 positionToHit = new Vector3(ballPos.x, ballPos.y, ballPos.z);

    	for(int i = 0; i < identities.Length; i++)
    	{
    		float minimRange = i * 90f;
    		//float dirMod = 1f;

    		if(angle >= minimRange && angle <= (minimRange + 90f)) //Checks the quadrant the angle belongs to...
    		{
    			//float quadrantVector = (identities[i].x * identities[i].z);
    			//dirMod = -1f; 

    			tempX = Mathf.Abs(Mathf.Cos(angle)) * identities[i].x * -1f;//To the contrary quadrant
    			tempZ = Mathf.Abs(Mathf.Sin(angle)) * identities[i].z * -1f;//To the contrary quadrant

    			positionToHit = new Vector3((positionToHit.x - (tempX * 2f)), positionToHit.y, (positionToHit.z - (tempZ * 2f)));

    			/*Debug.Log("Angle: " + angle);
    			Debug.Log("X: " + tempX);
    			Debug.Log("Z: " +tempZ);*/

    			return positionToHit;
    		}
    	}

    	return positionToHit;
    }

	///// Hit to the Goal /////
}