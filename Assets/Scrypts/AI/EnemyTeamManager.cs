﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyTeamManager : MonoBehaviour
{
	public List<GameObject> enemyTeam = new List<GameObject>();
	private List<AIStateMachine> aiStateMachines;
	private List<Vector3> initialPositions;
	private GameObject ball;

	public Transform goal; //Stores the goal that the team needs to get the ball to...

	private UIGameController uiGC;

	private int levelOneIndex;
	private int levelTwoIndex;
	private int levelThreeIndex;

	public Transform planeA;
	public Transform planeB;
	public Transform planeC;
	public Transform planeD;

	void Awake()
	{
		ball = this.GetComponent<GameMechanics>().getBall();
		aiStateMachines = new List<AIStateMachine>();
		initialPositions = new List<Vector3>();
		uiGC = this.GetComponent<UIGameController>();
		
		for(int i = 0; i < enemyTeam.Count; i++)
		{
			enemyTeam[i].GetComponent<PositionMovement>().setGoal(goal);
			aiStateMachines.Insert(i, enemyTeam[i].GetComponent<AIStateMachine>());
			initialPositions.Insert(i, enemyTeam[i].transform.position);
		}

		//setTeamState("Idle");
	}

	public void setOnInitialPositions()
	{
		for(int i = 0; i < enemyTeam.Count; i++)
		{
			enemyTeam[i].transform.position = initialPositions[i];
		}
	}

	public void setTeamParadigm(string _paradigm)
	{
		for(int i = 0; i < enemyTeam.Count; i++)
		{
			enemyTeam[i].GetComponent<AIStateMachine>().setParadigm(_paradigm);
		}
	}

	public List<GameObject> getEnemyTeam()
	{
		return this.enemyTeam;
	}

	public GameObject getEnemyTeamMemeber(int index)
	{
		return this.enemyTeam[index];
	}

	public void setTeamState(string _state)
	{
		for(int i = 0; i < enemyTeam.Count; i++)
		{
			//Debug.Log("Pretty Good! " +i);

			aiStateMachines[i].setCurrentState(_state);
		}

		uiGC.updateStatesText("Enemy", _state);

		//assignLevels();
	}

	void assignLevels()
	{
		float[] tempDistances = new float[enemyTeam.Count];

		/*int tempBest = 0;
		int tempLowest = 0;
		int tempMedium = 0;*/

		float maxDistance = 0.0f;
		float minDistance = 0.0f;

		for(int i = 0; i < enemyTeam.Count; i++)
		{
			tempDistances[i] =  new Vector2((enemyTeam[i].transform.position.x - ball.transform.position.x), enemyTeam[i].transform.position.z - ball.transform.position.z).magnitude;
		}

		maxDistance = Mathf.Max(tempDistances[0], Mathf.Max(tempDistances[1], tempDistances[2]));
		minDistance = Mathf.Min(tempDistances[0], Mathf.Min(tempDistances[1], tempDistances[2]));

		for(int i = 0; i < enemyTeam.Count; i++) //Asigna Maximo...
		{
			if(Mathf.Approximately(tempDistances[i], minDistance))
			{
				levelOneIndex = i;
			}
		}

		for(int i = 0; i < enemyTeam.Count; i++) //Asigna Menor...
		{
			if(Mathf.Approximately(tempDistances[i], maxDistance))
			{
				levelThreeIndex = i;
			}
		}

		for(int i = 0; i < enemyTeam.Count; i++) //Asigna Medio...
		{
			if(i != levelOneIndex && i != levelThreeIndex)
			{
				levelTwoIndex = i;
			}
		}

		//if(aiStateMachines[tempBest].getCurrentLevel() != "One")
		{
			enemyTeam[levelOneIndex].GetComponent<AIStateMachine>().setLevel("One");
			uiGC.updateLevelText(levelOneIndex, "Enemy", 1);
		}
		//if(aiStateMachines[tempMedium].getCurrentLevel() != "Two")
		{
			enemyTeam[levelTwoIndex].GetComponent<AIStateMachine>().setLevel("Two");
			uiGC.updateLevelText(levelTwoIndex, "Enemy", 2);
		}
		//if(aiStateMachines[tempLowest].getCurrentLevel() != "Three")
		{
			enemyTeam[levelThreeIndex].GetComponent<AIStateMachine>().setLevel("Three");
			uiGC.updateLevelText(levelThreeIndex, "Enemy", 3);
		}
		
	}

	public void setActivateMovement(bool _activate)
	{
		for(int i = 0; i < enemyTeam.Count; i++)
		{
			/*PositionMovement tempPM = */
			enemyTeam[i].GetComponent<PositionMovement>().enabled = _activate;
		}	
	}

	public int getPlayerWithLevelIndex(string _level)
	{
		GameObject tempPlayer = null;
		int _index = 0;

		switch(_level)
		{
			case "One":
			_index = enemyTeam.IndexOf(enemyTeam[levelOneIndex]);
			break;

			case "Two":
			_index = enemyTeam.IndexOf(enemyTeam[levelTwoIndex]);
			break;

			case "Three":
			_index = enemyTeam.IndexOf(enemyTeam[levelThreeIndex]);
			break;

			default:
			Debug.LogError("Level " +_level+ " not defined in Levels enumerator.");
			break;
		}

		return _index;
	}

	public Transform determinePlanePlayerIs(int _playerIndex)
    {
    	//Debug.Log("Checking the plane...");
    	int layerMask = LayerMask.GetMask("Plane Zones");

        Transform plane = null;

        Vector3 down = enemyTeam[_playerIndex].transform.TransformDirection(Vector3.down);
        RaycastHit hit;

        //Debug.DrawRay(enemyTeam[_playerIndex].transform.position, down * 10, Color.green);
        
        if(Physics.Raycast(enemyTeam[_playerIndex].transform.position, down, out hit, Mathf.Infinity, layerMask))
        {
            plane = hit.collider.transform;
            //Debug.Log("Name of the plane: " +plane.transform.name);    
        }
        else Debug.Log("Cannot fucking detect it...");

     	return plane;
    }

    public Transform getPlane(string _plane)
	{
		Transform tempPlane = null;

		switch(_plane)
		{
			case "A":
			tempPlane = planeA;
			break;

			case "B":
			tempPlane = planeB;
			break;

			case "C":
			tempPlane = planeC;
			break;

			case "D":
			tempPlane = planeD;
			break;

			default:
			Debug.LogError("Plane " +_plane+ " not defined in planes range.");
			break;
		}

		return tempPlane;
	}

	//Counts how many players are on certain plane...
	public int countPlayersOnPlane(string _plane)
	{
		Transform checkingPlane = getPlane(_plane);
		int playersCount = 0;

		for(int i = 0; i < enemyTeam.Count; i++)
		{
			if(determinePlanePlayerIs(i) == checkingPlane)
			{
				playersCount++;
			}
		}

		return playersCount;
	}

	void Update()
	{
		assignLevels();
	}
}
