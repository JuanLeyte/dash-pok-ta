﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerTeamManager : MonoBehaviour
{
	public List<GameObject> playerTeam = new List<GameObject>();
	private List<AIStateMachine> aiStateMachines;
	private List<Vector3> initialPositions;
	private GameObject ball;
	private SelectedPlayerManager spm;

	public Transform goal; //Stores the goal that the team needs to get the ball to...

	private UIGameController uiGC;

	private int levelOneIndex;
	private int levelTwoIndex;
	private int levelThreeIndex;

	public Transform planeA;
	public Transform planeB;
	public Transform planeC;
	public Transform planeD;

	// Use this for initialization
	void Awake()
	{
		ball = this.GetComponent<GameMechanics>().getBall();
		aiStateMachines = new List<AIStateMachine>();
		initialPositions = new List<Vector3>();
		uiGC = this.GetComponent<UIGameController>();
		spm = this.GetComponent<SelectedPlayerManager>();

		for(int i = 0; i < playerTeam.Count; i++)
		{
			playerTeam[i].GetComponent<PositionMovement>().setGoal(goal);
			aiStateMachines.Insert(i, playerTeam[i].GetComponent<AIStateMachine>());
			initialPositions.Insert(i, playerTeam[i].transform.position);
		}

		//setTeamState("Idle");
	}

	public void setOnInitialPositions()
	{
		for(int i = 0; i < playerTeam.Count; i++)
		{
			playerTeam[i].transform.position = initialPositions[i];
		}
	}

	public List<GameObject> getPlayerTeam()
	{
		return this.playerTeam;
	}

	public GameObject getPlayerTeamMember(int index)
	{
		return this.playerTeam[index];
	}

	public void setTeamParadigm(string _paradigm)
	{
		for(int i = 0; i < playerTeam.Count; i++)
		{
			playerTeam[i].GetComponent<AIStateMachine>().setParadigm(_paradigm);
		}
	}

	public void setTeamState(string _state)
	{
		for(int i = 0; i < playerTeam.Count; i++)
		{
			//Debug.Log("Pretty Good! " +i);

			aiStateMachines[i].setCurrentState(_state);
		}

		uiGC.updateStatesText("Player", _state);

		//assignLevels();
	}

	void assignLevels()
	{
		float[] tempDistances = new float[playerTeam.Count];

		/*int tempBest = 0;
		int tempLowest = 0;
		int tempMedium = 0;*/

		float maxDistance = 0.0f;
		float minDistance = 0.0f;

		for(int i = 0; i < playerTeam.Count; i++)
		{
			tempDistances[i] =  new Vector2((playerTeam[i].transform.position.x - ball.transform.position.x), playerTeam[i].transform.position.z - ball.transform.position.z).magnitude;
		}

		maxDistance = Mathf.Max(tempDistances[0], Mathf.Max(tempDistances[1], tempDistances[2]));
		minDistance = Mathf.Min(tempDistances[0], Mathf.Min(tempDistances[1], tempDistances[2]));

		for(int i = 0; i < playerTeam.Count; i++) //Asigna Maximo...
		{
			if(Mathf.Approximately(tempDistances[i], minDistance))
			{
				levelOneIndex = i;
			}
		}

		for(int i = 0; i < playerTeam.Count; i++) //Asigna Menor...
		{
			if(Mathf.Approximately(tempDistances[i], maxDistance))
			{
				levelThreeIndex = i;
			}
		}

		for(int i = 0; i < playerTeam.Count; i++) //Asigna Medio...
		{
			if(i != levelOneIndex && i != levelThreeIndex)
			{
				levelTwoIndex = i;
			}
		}

		//if(aiStateMachines[tempBest].getCurrentLevel() != "One")
		{
			playerTeam[levelOneIndex].GetComponent<AIStateMachine>().setLevel("One");
			uiGC.updateLevelText(levelOneIndex, "Player", 1);
			//spm.switchPlayer(tempBest);
		}
		//if(aiStateMachines[tempMedium].getCurrentLevel() != "Two")
		{
			playerTeam[levelTwoIndex].GetComponent<AIStateMachine>().setLevel("Two");
			uiGC.updateLevelText(levelTwoIndex, "Player", 2);
		}
		//if(aiStateMachines[tempLowest].getCurrentLevel() != "Three")
		{
			playerTeam[levelThreeIndex].GetComponent<AIStateMachine>().setLevel("Three");
			uiGC.updateLevelText(levelThreeIndex, "Player", 3);
		}


		//Selected Player EXclusivity Section...
		if(spm.getPlayerIndex() != levelOneIndex) //If the player with the level 1 is not the current selected player, it checks the exclusivity...
		{
			PositionMovement tempPP = playerTeam[levelOneIndex].GetComponent<PositionMovement>();

			//If both clashing players are on the documented circular area, it applies the selected player exclusivity...
			if(spm.getPlayerDistance(spm.getPlayerIndex()) >= 5f && spm.getPlayerDistance(spm.getPlayerIndex()) <= 10f &&
			spm.getPlayerDistance(levelOneIndex) >= 5f && spm.getPlayerDistance(levelOneIndex) <= 10f)
			{
				if(tempPP.getPlayerExclusivity())
				tempPP.setPlayerExclusivity(true);
			}
			else
			{
				if(tempPP.getPlayerExclusivity())
				tempPP.setPlayerExclusivity(false);
			}
		}
	}

	public void setActivateMovement(bool _activate)
	{
		for(int i = 0; i < playerTeam.Count; i++)
		{
			if(this.GetComponent<SelectedPlayerManager>().getPlayerIndex() != i)
			/*PositionMovement tempPM = */
			playerTeam[i].GetComponent<PositionMovement>().enabled = _activate;
		}
	}

	/*public GameObject getPlayerWithLevel(string _level)
	{
		for(int = 0; i < playerTeam.Count; i++)
		{
			if(playerTeam[i].getLevel() == _level)
			{
				return playerTeam[i];
			}
		}

		return null;
	}*/

	public int getPlayerWithLevelIndex(string _level)
	{
		GameObject tempPlayer = null;
		int _index = 0;

		switch(_level)
		{
			case "One":
			_index = playerTeam.IndexOf(playerTeam[levelOneIndex]);
			break;

			case "Two":
			_index = playerTeam.IndexOf(playerTeam[levelTwoIndex]);
			break;

			case "Three":
			_index = playerTeam.IndexOf(playerTeam[levelThreeIndex]);
			break;

			default:
			Debug.LogError("Level " +_level+ " not defined in Levels enumerator.");
			break;
		}

		return _index;
	}

	public Transform determinePlanePlayerIs(int _playerIndex)
    {
    	//Debug.Log("Checking the plane...");
    	int layerMask = LayerMask.GetMask("Plane Zones");

        Transform plane = null;

        Vector3 down = playerTeam[_playerIndex].transform.TransformDirection(Vector3.down);
        RaycastHit hit;

        //Debug.DrawRay(playerTeam[_playerIndex].transform.position, down * 10, Color.green);
        
        if(Physics.Raycast(playerTeam[_playerIndex].transform.position, down, out hit, Mathf.Infinity, layerMask))
        {
            plane = hit.collider.transform;
            //Debug.Log("Name of the plane: " +plane.transform.name);    
        }
        else Debug.Log("Cannot fucking detect it...");

     	return plane;
    }

	public Transform getPlane(string _plane)
	{
		Transform tempPlane = null;

		switch(_plane)
		{
			case "A":
			tempPlane = planeA;
			break;

			case "B":
			tempPlane = planeB;
			break;

			case "C":
			tempPlane = planeC;
			break;

			case "D":
			tempPlane = planeD;
			break;

			default:
			Debug.LogError("Plane " +_plane+ " not defined in planes range.");
			break;
		}

		return tempPlane;
	}

	//Used to test the determinePlanePlayerIs() function...
	public Transform getReferencePlane(Transform _plane)
	{
		Transform tempPlane = null;

		if(_plane == planeA)
		{
			tempPlane = planeA;
		}

		else if(_plane == planeB)
		{
			tempPlane = planeB;
		}

		else if(_plane == planeC)
		{
			tempPlane = planeC;
		}

		else if(_plane == planeD)
		{
			tempPlane = planeD;
		}
		else
		{
			Debug.LogError("Plane not defined in planes range.");
		}

		return tempPlane;
	}

	//Counts how many players are on certain plane...
	public int countPlayersOnPlane(string _plane)
	{
		Transform checkingPlane = getPlane(_plane);
		int playersCount = 0;

		for(int i = 0; i < playerTeam.Count; i++)
		{
			if(determinePlanePlayerIs(i) == checkingPlane)
			{
				playersCount++;
			}
		}

		return playersCount;
	}

	void Update()
	{
		assignLevels();
	}
}
