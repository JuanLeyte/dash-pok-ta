﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
 
 public class AIBehavior : MonoBehaviour
 {
 	const float Interval = 2;
 	const float interval = 4; // only allow swapping after 5 seconds
	float lastTime;
	float lasttime;
    public float movementSpeed = 10;
    private GameObject ball;
    private float distance;
    public GameObject DirectionObject;

    private PositionMovement pm;
    private AIStateMachine aism;

    private Animator anim;

    bool dashing;
    bool idle;
    bool jumping;
    bool run;
     
     void Awake()
     {
        anim = this.GetComponent<Animator>();
        dashing = false;
        run = false;
        idle = false;
        jumping = false;
        pm = this.GetComponent<PositionMovement>();
        aism = this.GetComponent<AIStateMachine>();
        ball = pm.getBall(); //Gets the ball from the PositionMovement script, to avoid redundance on the editor...
     }
     
     void Update()
     {
        //distance = new Vector2(this.transform.position.x - ball.transform.position.x, this.transform.position.z - ball.transform.position.z).magnitude;
        if(GetComponent<Rigidbody>().IsSleeping()) //No movement on rigidbody...
        {
            if(idle == false)
            {
                idle = true;
                run = false;
                anim.SetBool("idle", idle);
            }
        }
        else
        {
            if(run == false)
            {
                idle = false;
                run = true;
                anim.SetBool("run", run);
            }
        }

        if (pm.getDistance() < 1f && ball.transform.position.y < 9f)
		{
            //Debug.Log("Close Enough...");
			//pm.setOnBallRange(true);
            if(!dashing)
            {
                Debug.Log("Should be dashing");
                dashing = true;
                anim.SetBool("dash", dashing);
                //lastTime = Time.time;
                //GetComponent<Rigidbody>().AddForce(transform.forward * 50, ForceMode.Impulse);
                this.gameObject.GetComponent<Rigidbody>().AddForce(transform.forward * 50, ForceMode.Impulse);
                //this.gameObject.GetComponent<Rigidbody>().AddRelativeForce(transform.forward * 15);
                //aism.setStatus("Done");
                StartCoroutine(coolDown());
            }
            else
            {
                //aism.setStatus("Failed");
            	//pm.setOnBallRange(true);
            }
            /*if(ball.transform.position.y > 1.5)
            {
                if(!jumping)
                {
                    if(GetComponent<PlayersStats>().isOnGround())
                    {
                        jumping = true;
                        anim.SetBool("jump", jumping);
                        GetComponent<Rigidbody>().AddForce(transform.up * 20, ForceMode.VelocityChange);
                    }
                }
                else
                {
                    if(!GetComponent<PlayersStats>().isOnGround())
                    {
                        jumping = false;
                        anim.SetBool("jump", jumping);
                    }
                }
            }*/

            /*aism.setCurrentState("Unasigned");
            aism.setStatus("Unasigned");
            StartCoroutine(finishAtacking());*/
		}
     }

     string determinePlanePlayerIs()
     {
     	string planeName = "";

     	RaycastHit hit;
     	Vector3 down = transform.TransformDirection(Vector3.down);

     	if(Physics.Raycast(transform.position, down, Mathf.Infinity, 10))
     	{
     		planeName = hit.transform.name;
     		Debug.Log("Name of the plane: " +planeName);
     	}

     	return planeName;
     }

     IEnumerator coolDown()
     {
        anim.SetBool("dash", dashing);
        dashing = false;
        yield return new WaitForSeconds(1.5f);
        dashing = true;
        
     }

     //Makes the player heads to the direction the ball is...
    public float headOnBall()
    {
        Vector3 playerPos = this.transform.position;
        Vector3 ballPos = ball.transform.position;

        float angle;
        float x = (playerPos.x - ballPos.x);
        float z = (playerPos.z - ballPos.z);

        if(playerPos.z < ballPos.z || playerPos.x < ballPos.x && playerPos.z < ballPos.z)
        {
            angle = (Mathf.Atan2(z, x) + (Mathf.PI * 2)) * Mathf.Rad2Deg;
        }
        else
        {
            angle = Mathf.Atan2(z, x) * Mathf.Rad2Deg;
        }

        return angle;
    }

    public void finishAtackig()
    {
        StartCoroutine(finishAtacking());
    }

    IEnumerator finishAtacking()
    {
        pm.enabled = false;

        yield return new WaitForSeconds(0.75f);

        pm.enabled = true;
        //aism.letKnowAllTeammatesStateChange(aism.getCurrentState());
        aism.setCurrentState("Unasigned");
    }
 }
