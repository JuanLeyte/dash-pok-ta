﻿ using UnityEngine;
 using System.Collections;
 
 public class PlayerMove :  MonoBehaviour
 {
 	const float Interval = 1.5f;
 	const float interval = 0.5f; 
	float lastTime;
	float lasttime;
    Rigidbody rigidbody;
    Vector3 posRigid;
    PlayersStats playersStatsScrypts;
    bool rotating;

/*   public float accelerationZ;
     public float maxSpeed;*/
     public float rotateLeftP;
     public float rotateRightP;
     public float acel;

     private Vector3 movement;
     private Vector3 identitie;

     private Vector3 cameraForward;
     private Vector3 cameraRigth;

     private Animator anim;

    // private float distToGround;

     float h;
     float v;
 
     void Start ()
     {
        cameraForward = Camera.main.transform.TransformDirection(Vector3.forward);
        cameraForward.y = 0;
        cameraForward = cameraForward.normalized;
        cameraRigth  = new Vector3(cameraForward.z, 0, cameraForward.x);

        anim = this.GetComponent<Animator>();

        h = 0f;
        v = 0f;
        movement = new Vector3(1f, 0f, 1f);
        playersStatsScrypts = GetComponent<PlayersStats>();
        rigidbody = this.GetComponent<Rigidbody>();
        acel = 10;
        rotating = false;

        identitie = new Vector3(h, 0, v);
        //transform.forward = Vector3.Normalize(new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"))); 
     }
     
     void Update ()
     {

        h = Input.GetAxis("Horizontal");//thies work with WASD or arrow keys + other things unity can use, gamepad etc..
        v = Input.GetAxis("Vertical");

        identitie.x = h;
        identitie.z = v;

        //Debug.Log("H: " +h);
        //Debug.Log("V: " +v);

        if(v != 0 || h != 0)
        {// if we are getting inputs, form -1 to 1, 0=no input
            Vector3 direction = (cameraRigth *  h) + (cameraForward * v);// as the key is held down the h or v values will move toward full -1 or 1, thus handling the smooth rotation
            transform.rotation = Quaternion.LookRotation(direction);
            anim.SetBool("run", true);
            moveForward();
        }
        else
        {
            anim.SetBool("run", false);
        }

        if (Input.GetButton("Dash")&& Time.time - lastTime >= Interval)
		{
            Debug.Log("Player Dashing");
            anim.SetBool("dash", true);
			lastTime = Time.time;
            GetComponent<Rigidbody>().AddForce((cameraRigth *  h) + (cameraForward * v) *  15, ForceMode.VelocityChange);
			//GetComponent<Rigidbody>().AddForce(identitie * 10, ForceMode.VelocityChange);
			
		}
        else
        {
            anim.SetBool("dash", false);
        }
		if (Input.GetButton("Jump") && Time.time - lasttime >= interval)
		{
            if(playersStatsScrypts.isOnGround())
            {
                lasttime = Time.time;

                anim.SetBool("jump", true);

                GetComponent<Rigidbody>().AddForce(transform.up * 10, ForceMode.VelocityChange);
            }
			else
            {
                Debug.Log("Not in ground...");
            }
		}
        else
        {
            anim.SetBool("jump", false);
        }

     }


     IEnumerator rotateTowards(float angle)
     {
        rotating = true;
        Debug.Log("Rotating the mono...");
        Debug.Log("Your angle: " +transform.eulerAngles.y);
        float turningTime = 0.8f;
        //float thisAngle = this.transform.eulerAngles.y;
        Vector3 thisAngle = this.transform.eulerAngles;

        if(thisAngle.y < angle)
        {
            while(thisAngle.y < angle)
            {
                thisAngle.y += turningTime * Time.deltaTime;
                this.transform.eulerAngles = thisAngle;
            }
        }

        if(thisAngle.y > angle)
        {
            while(thisAngle.y > angle)
            {
                thisAngle.y -= turningTime * Time.deltaTime;
                this.transform.eulerAngles = thisAngle;
            }
        }
        

        
        this.transform.eulerAngles = thisAngle;

        rotating = false;

        yield return null;

     }


     public void moveForward()
     {
        if(playersStatsScrypts.getAccelerationZ() >= playersStatsScrypts.getMaxSpeed())
        {
            playersStatsScrypts.setAccelerationZ(15f);         
        }
        else if(playersStatsScrypts.getAccelerationZ() <= playersStatsScrypts.getMaxSpeed())
        {
            playersStatsScrypts.accelerateZ(acel*Time.deltaTime);
        }
        if(playersStatsScrypts.getAccelerationX() >= playersStatsScrypts.getMaxSpeed())
        {
            playersStatsScrypts.setAccelerationX(15f);
        }
        else if(playersStatsScrypts.getAccelerationX() <= playersStatsScrypts.getMaxSpeed())
        {
            playersStatsScrypts.accelerateX(acel*Time.deltaTime);
        }

        //Vector3 mov = new Vector3((playersStatsScrypts.getAccelerationX() * v), 0f, (playersStatsScrypts.getAccelerationZ()) * h) * Time.deltaTime;
        //Vector3 dir = (cameraRigth * v + cameraForward * h);

        //transform.localPosition = Vector3.MoveTowards(transform.position, transform.position + mov, Time.deltaTime);
        //transform.Translate(mov, 0);
        //transform.position += mov /*+ dir*/ * 0.8f;
        transform.Translate(-cameraForward * Time.deltaTime * playersStatsScrypts.getAccelerationZ(), Space.Self);
     }

     public float getVel()
     {
        return playersStatsScrypts.getAccelerationZ();
     }
}